/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <iostream>
#include <cstdlib>
#include "globals.h"
#include "baselines.h"
using namespace std;

Baselines::Baselines() {
  int bl_index = 0;

  codes = (float*)malloc(NUM_BASELINES*sizeof(float));

  for (int st1=0; st1<NUM_STANDS; ++st1) 
    for(int st2=st1; st2<NUM_STANDS; ++st2) {
      indexes[st1][st2] = bl_index;
      codes[bl_index] = encodeBaseline(st1, st2); 
      ++bl_index;
    }
}

int Baselines::baseline_index(int stand1_index, int stand2_index) {
  int bl_index=0;
  int tmp;

  //cout << stand1_index << " " << stand2_index << endl;

  if ( stand1_index > stand2_index ) {
    tmp = stand1_index;
    stand1_index = stand2_index;
    stand2_index = tmp;
  }

  if ( stand1_index < 0 || NUM_STANDS <= stand1_index || stand2_index < 0 || NUM_STANDS <= stand2_index ) {
    cerr << "Invalid stand number to baseline index lookup. S1: " << stand1_index << " S2: " << stand2_index << endl;
    exit(1);
  }
  return indexes[stand1_index][stand2_index];
}

int Baselines::encodeBaseline(int stand1_index, int stand2_index) {
  int i, tmp;

  if ( stand1_index < 0 || NUM_STANDS <= stand1_index || stand2_index < 0 || NUM_STANDS <= stand2_index ) {
    cerr << "Invalid stand number to baseline encoding. S1: " << stand1_index << " S2: " << stand2_index << endl;
    exit(1);
  }

  if ( stand1_index > stand2_index ) {
    tmp = stand1_index;
    stand1_index = stand2_index;
    stand2_index = tmp;
  }

  ++stand1_index; ++stand2_index;		// encoding uses 1-based indexing

  if (stand2_index > 255) {
    return stand1_index*2048 + stand2_index + 65536;
  } else {
    return stand1_index*256 + stand2_index;
  }

}

void Baselines::decodeBaseline(int blcode, int *b1, int *b2) {
  if (blcode > 65536) {
    blcode -= 65536;
    *b2 = (int) blcode % 2048 -1;   // to 0-based
    *b1 = (blcode - *b2)/2048 -1;
  }
  else {
    *b2 = (int) blcode % 256 -1;	
    *b1 = (blcode - *b2)/256 -1;
  }
}



/*

int main() {
  Baselines bl;
  int code, a1, a2;

  for (int st1=0; st1<NUM_STANDS; ++st1) 
    for(int st2=st1; st2<NUM_STANDS; ++st2) cout << bl.baseline_index(st1, st2) << endl;   // baselines indexes in sequence

  cout << bl.encodeBaseline(16,111) << endl;	// 4464
  cout << bl.encodeBaseline(200,202) << endl;	// 51659

  for (int st1=0; st1<NUM_STANDS; ++st1) 
    for(int st2=st1; st2<NUM_STANDS; ++st2) {
      a1 = st1; a2 = st2;
      bl.decodeBaseline(bl.encodeBaseline(st1, st2), &a1, &a2);
      if ( a1 != st1 || a2 != st2 ) {
        cerr << "Baseline coding doesn't work " << st1 << " " << st2 << " " << a1 << " " << a2 << endl;
	exit(1);
      }
  }

  bl.decodeBaseline(bl.codes[9543], &a1, &a2);
  cout << a1 << " " << a2 << " " << bl.baseline_index(a1, a2) << endl;

  return 0;
}
*/
