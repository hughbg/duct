/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
  Put together the visibilities and UVWs. Read from the DADA file and
  use metadata. Add cable delays to visibilities if required. Precess antenna locations,
  if required, before calculating UVW from them.
*/

#include <iostream>
#include <iomanip>
#include "pal.h"
#include "baselines.h"
#include "flags.h"
#include "uvdata.h"
#include "dada_reader.h"
#include "globals.h"
#define TRUE 1

UVWdata::UVWdata(Header& header, lockActions whatLock, const int what_to_load) {
  double jdtime_base;
  int res;

  name = header.field_name;
  n_scans = header.n_scans;
  n_chans = header.n_chans;

  cent_freq_Hz = header.cent_freq_MHz*1e6;
  channel_width_Hz = header.bandwidth_MHz/(n_chans)*1e6*(header.invert_freq ? -1.0: 1.0);

  /* extract RA, DEC from header. Beware negative dec and negative zero bugs. */
  ra_hrs = header.ra_hrs;
  dec_degs = header.dec_degs;

  dates = (double*)malloc(n_scans*sizeof(double));
  /* calculate the JD of the beginning of the data */
  palCaldj(header.year, header.month, header.day, &jdtime_base, &res); // get MJD for calendar day of obs
  jdtime_base += 2400000.5;  // convert MJD to JD
  jdtime_base += header.hour/24.0+header.minute/1440.0+header.second/86400.0; // add intra-day offset
  dates[0] = jdtime_base+0.5*(header.integration_time/86400.0);
  for (int i=1; i<n_scans; ++i) 
    dates[i] = dates[0] + i*(header.integration_time/86400.0);

  visdata = NULL; weightdata = NULL;
  u = v = w = NULL;

  true_scan = ALL_SCANS;
  if ( what_to_load != ALL_SCANS && what_to_load != HEADER_ONLY ) {			 // pretend we are only looking at one scan, what_to_load is it
    if ( what_to_load > n_scans-1 ) {		 // don't forget to tell dada_reader
      cerr << "Scan for what_to_load is out of range: " << what_to_load << endl;
      exit(1);
    }
    n_scans = 1;
    if ( whatLock == noLockPointing ) dates[0] = dates[what_to_load];
    true_scan = what_to_load;
  }
}


int UVWdata::codePols(const int pol1, const int pol2) {
  if (pol1==0 && pol2==0) return 0;
  if (pol1==1 && pol2==1) return 1;
  if (pol1==0 && pol2==1) return 2;
  if (pol1==1 && pol2==0) return 3;
  return -1;
}

void UVWdata::make_uvw(Cables& cables, Stands& stands, const standActions whatTransform, const lockActions locking) {
  int bl_index, stand1, stand2, temp;

  if ( u == NULL ) { 
    u = (double**)malloc(n_scans*sizeof(double*));
    for (int scan_index=0; scan_index<n_scans; ++scan_index) u[scan_index] = (double*)malloc(NUM_BASELINES*sizeof(double));
  }
  if ( v == NULL ) { 
    v = (double**)malloc(n_scans*sizeof(double*));
    for (int scan_index=0; scan_index<n_scans; ++scan_index) v[scan_index] = (double*)malloc(NUM_BASELINES*sizeof(double));
  }
  if ( w == NULL ) { 
    w = (double**)malloc(n_scans*sizeof(double*));
    for (int scan_index=0; scan_index<n_scans; ++scan_index) w[scan_index] = (double*)malloc(NUM_BASELINES*sizeof(double));
  }

  for (int scan_index=0; scan_index<n_scans; ++scan_index) {
    Stands op_stands(stands);   // so we don't alter the incoming stands
    op_stands.transform(locking==doLockPointing?dates[0]:dates[scan_index], ra_hrs, dec_degs, whatTransform);

    for (int inp1=0; inp1<cables.length(); ++inp1)
      for (int inp2=inp1; inp2<cables.length(); ++inp2) {   

         /* decode the inputs into antennas and pols */

        stand1 = cables[inp1].stand_num();
        stand2 = cables[inp2].stand_num();
       /* UVFITS by convention expects the index of stand2 to be greater than stand1, so swap if necessary */
        if ( stand1 > stand2 ) {
            temp = stand1;
            stand1 = stand2;
            stand2 = temp;
        }

        bl_index = baselines.baseline_index(stand1, stand2);
	
        if(  stand1 == stand2  ) { u[scan_index][bl_index] = v[scan_index][bl_index] = w[scan_index][bl_index] = 0; }
        else {
          u[scan_index][bl_index] = (op_stands[stand1].east_x - op_stands[stand2].east_x)/VLIGHT;
          v[scan_index][bl_index] = (op_stands[stand1].north_y - op_stands[stand2].north_y)/VLIGHT;
          w[scan_index][bl_index] = (op_stands[stand1].height_z - op_stands[stand2].height_z)/VLIGHT;
	  //if ( scan_index == 0 ) cout << "UVW " << stand1 << " " << stand2 << " " << u[scan_index][bl_index]*VLIGHT << " " << v[scan_index][bl_index]*VLIGHT << endl;
        }
      }
  }
}


void UVWdata::load_visibilities(const char *filename, Cables& cables, Header& header, const cableActions to_do, const float scale_factor) {
  int bl_index, stand1, stand2, stand1_pol, stand2_pol, temp;
  complex<float> visibility;
  double cable_delay, frequency, lambda;
  float this_w;
  int pol_ind, visindex;
  bool baseline_reverse;
  complex<float> phase;
  DadaReader dada_reader(filename, true_scan, scale_factor);
  //Flags flags("flags.txt");
  bool warned = false;

  /*ifstream file("flagged.dat"); 
  string line;
  vector <string> words;
  int i=0;
  if ( file.is_open() ) {
    while( getline(file, line) ) {
      words = split(line); 
      flags[atoi(words[0].c_str())][atoi(words[1].c_str())][atoi(words[2].c_str())][atoi(words[3].c_str())] = 1;
      cout << atoi(words[0].c_str()) << " " << atoi(words[1].c_str()) << " " << atoi(words[2].c_str()) << " " << atoi(words[3].c_str())<<endl;
      ++i;
    }
  }
  cout << "Flags " << i << endl;*/

  if ( visdata == NULL ) {
    visdata = (std::complex<float>**)malloc(n_scans*sizeof(std::complex<float>*)); 
    for (int i=0; i<n_scans; ++i) visdata[i] = (std::complex<float>*)malloc(NUM_BASELINES*n_chans*NUM_POLS*sizeof(std::complex<float>));
    weightdata = (float**)malloc(n_scans*sizeof(float*)); 
    for (int i=0; i<n_scans; ++i) weightdata[i] = (float*)malloc(NUM_BASELINES*n_chans*NUM_POLS*sizeof(float));
  }

  for (int time_index=0; time_index<n_scans; ++time_index) {
    dada_reader.next_time_scan();
    cout << "Loading time " << time_index;
    if ( true_scan != ALL_SCANS ) cout << " (" << true_scan << ")";
    cout << endl;

    for (int chan=0; chan<n_chans; ++chan) {
      for(int inp1=0; inp1<cables.length(); inp1++) {
        for(int inp2=inp1; inp2<cables.length(); inp2++) {

          /* decode the inputs into antennas and pols */
          baseline_reverse = false;
          stand1 = cables[inp1].stand_num();
          stand2 = cables[inp2].stand_num();
          stand1_pol = cables[inp1].pol_index();
          stand2_pol = cables[inp2].pol_index();
          /* UVFITS by convention expects the index of stand2 to be greater than stand1, so swap if necessary */
          if ( stand1 > stand2 ) {    // Currently only happening for stands >= 248
              temp = stand1;
              stand1 = stand2;
              stand2 = temp;
              temp = stand1_pol;
              stand1_pol = stand2_pol;
              stand2_pol =temp;
              baseline_reverse = true;
          }
 
          bl_index = baselines.baseline_index(stand1, stand2);  
          pol_ind = codePols(stand1_pol, stand2_pol);
	  visindex = bl_index*NUM_POLS*n_chans + chan*NUM_POLS + pol_ind;

	  if ( TRUE || stand2 < NUM_INRIGGERS ) {		// is not an outrigger baseline. Just get the zeros for now.
		// indexes are swapped. See issues #7,8.
	    visibility = dada_reader.get_vis_value(chan, stand2, stand1, stand2_pol, stand1_pol);	
	  } else { 
	    visibility = dada_reader.get_outrigger_value(0, chan, dada_reader.outrigger_index(stand1, stand2), stand1_pol, stand2_pol);
		
	  }
	  if ( header.conjugate ) visibility = conj(visibility);    

	  if ( to_do == doCableDelay ) {
	    if ( w == NULL && !warned ) {
	      cerr << "WARNING: w is not calculated, but needed for cable delay. Using 0.\n";
	      warned = true;
	      this_w = 0.0;
	    } else {    // Note: this w has not been precessed. In corr2uvfits, it is already precessed.
	      this_w = w[time_index][bl_index]*VLIGHT; if ( !header.geom_correct ) this_w = 0.0;      
	    }     

	    // cable delay only works when vis is conjugated
            cable_delay = cables[inp2].cable_length()-cables[inp1].cable_length();
             /* calc wavelen for this channel in metres. header freqs are in MHz*/
            frequency = header.cent_freq_MHz + (header.invert_freq? -1.0:1.0)*(chan - n_chans/2)*(header.bandwidth_MHz/n_chans);
            //frequency = header.cent_freq_MHz + (header.invert_freq? -1.0:1.0)*(chan - n_chans/2.0)/n_chans*header.bandwidth_MHz;   // don't do it like this
            lambda = (VLIGHT/1e6)/frequency;
            phase.real() = 0; phase.imag() = (-2.0*M_PI)*(this_w+cable_delay*(baseline_reverse? -1.0:1.0))/lambda;
	    phase = exp(phase);
            if ( baseline_reverse ) visibility = conj(visibility);
	    visibility *= phase;
            
          }

          // The combination of slight change to frequency calculation and the w values not being precessed can produce slightly
	  // different visibilities to corr2uvfits.

	  visdata[time_index][visindex] = visibility;

	  if ( abs(visibility) == 0 || cables.find_cable_flag(stand1, stand1_pol) != 0 || cables.find_cable_flag(stand2, stand2_pol) != 0  ) {
	    weightdata[time_index][visindex] = -header.integration_time;
	  } else weightdata[time_index][visindex] = header.integration_time;
   

        }

      }
    }  


  }

}

complex<float> UVWdata::get_vis(const int time, const int baseline, const int chan, const int pol1, const int pol2) {
  int pol_ind = codePols(pol1, pol2);
  return visdata[time][baseline*NUM_POLS*n_chans + chan*NUM_POLS + pol_ind];
}
        
void UVWdata::print(const int scan) {
  cout << "Name: " << name << " N scans: " << n_scans;
  cout << " Cent freq (Hz): " << cent_freq_Hz << " Channel width (Hz): " << channel_width_Hz;
  cout << " RA (deg): " << raDeg() << " Dec (deg): " << dec_degs << endl;
  cout << "Julian Dates:";
  cout.setf(ios::fixed);
  for (int i=0; i<n_scans; ++i) cout << " " << dates[i];
  cout << endl;
  
  cout.precision(10);
  if ( scan < 0 || n_scans <= scan ) 
    cerr <<"Cannot print uvw, scan is out of range\n";
  else 
    for (int i=0; i<5; ++i) cout << i << " " << u[scan][i]*VLIGHT << " " << v[scan][i]*VLIGHT << " " << w[scan][i]*VLIGHT << endl;
}


UVWdata::~UVWdata() {
  if ( u != NULL ) {
    for (int i=0; i<n_scans; ++i) { 
      free(u[i]); free(v[i]); free(w[i]); 
    }
    free(u); free(v); free(w);
  }
  if ( visdata != NULL ) { 
    for (int i=0; i<n_scans; ++i) { 
      free(visdata[i]); free(weightdata[i]); 
    }
    free(weightdata); free(visdata);
  }    
  if (dates != NULL ) free(dates);
}
