#include <iostream>
#include "globals.h"
#include "baselines.h"
#include "duct.h"
#include "duct_light.h"

static float uv_list[(NUM_BASELINES-NUM_OUTRIGGER_BASELINES)*6];     // baselines excluding outriggers, 6 values stand1 stand2 U V Vis(Re) Vis(Im)

static void check_andfree(void *p) {
  if ( p != NULL ) free(p);
}

float *load_dada_light(const char *file_name) {
  uvdata *data;
  Baselines bl;
  int vis_index, bl_index, chan, pol_ind;

  import_telescope_observation(&data, "header.txt", "antenna_locations.txt", "instr_config.txt", file_name, 0, 1);   // only first dump

  for (int i=0; i<NUM_BASELINES-NUM_OUTRIGGER_BASELINES; ++i) {
    int st1, st2;
    
    bl.decodeBaseline((int)data->baseline[0][i], &st1, &st2);
    uv_list[i*6+0] = st1; 
    uv_list[i*6+1] = st2; 

    uv_list[i*6+2] = data->u[0][i]*VLIGHT;
    uv_list[i*6+3] = data->v[0][i]*VLIGHT;

    bl_index = i;
    chan = 50;
    pol_ind = 0;
    vis_index = bl_index*NUM_POLS*data->n_freq+chan*NUM_POLS+pol_ind;
    vis_index *= 2; 	// 2 floats per complex
    uv_list[i*6+4] = data->visdata[0][vis_index];
    uv_list[i*6+5] = data->visdata[0][vis_index+1];
  }

  check_andfree(data->date);
  check_andfree(data->n_baselines);
  check_andfree(data->source);
  check_andfree(data->array->antennas);
  check_andfree(data->array);
  for (int i=0; i<data->n_vis; ++i) {
    if ( data->u != NULL ) check_andfree(data->u[i]); if ( data->v != NULL )  check_andfree(data->v[i]); if ( data->v != NULL )  check_andfree(data->w[i]);
    if ( data->visdata != NULL )  check_andfree(data->visdata[i]); if ( data->weightdata != NULL ) check_andfree(data->weightdata[i]);
    if ( data->baseline != NULL )  check_andfree(data->baseline[i]);
  }
  check_andfree(data->u); check_andfree(data->v); check_andfree(data->w);
  check_andfree(data->visdata); check_andfree(data->weightdata);
  check_andfree(data->baseline);
  check_andfree(data);

  return uv_list;
  
}



#ifdef TEST
using namespace std;

// Testng
int main() {
  int index = 9543;
  float *uv_data = load_dada_light("2015-04-08-20_15_03_0001133593833216.dada");

  cout << uv_data[index*6] << " " << uv_data[index*6+1] << " " << uv_data[index*6+2] << " " << uv_data[index*6+3] << " " << uv_data[index*6+4] << " " << uv_data[index*6+5] << endl;
} 

#endif
