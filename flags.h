/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef FLAGS_H
#define FLAGS_H

/*
 Load flags. Usually data comes from flags.txt.
*/


#include <vector>
#include "header.h"
#include "globals.h"

using namespace std;

// Collection of all flags
class Flags {
private:
  typedef bool scan[NUM_STANDS][NUM_STANDS][NUM_CHANS];
  scan *flags;         // baselines

public:

  Flags(const char* filename);
  Flags() {}

  bool flagged(int time, int stand1, int stand2, int chan) { return flags[time][stand1][stand2][chan] || flags[time][stand2][stand1][chan]; }
  void print() { for (int t=0; t<10; ++t) for (int i=0; i<NUM_STANDS; ++i) for (int j=0; j<NUM_STANDS; ++j) for (int ch=0; ch<NUM_CHANS; ++ch) if ( flags[t][i][j][ch] ) cout << t << " " << i << " " << j << " " << ch << endl; }
};
#endif
