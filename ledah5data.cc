#include <hdf5.h>
#include <hdf5_hl.h>
#include <iostream>
#include <cstdlib>
#include <vector>
#include "ledah5data.h"
#include "globals.h"

using namespace std;
#define NUM_FLOATS_PER_COMPLEX 2


// all polymorphic gets
void get_val(hid_t id, const char *name, double& val) {
  char attr_val[256];
  if ( H5LTget_attribute_string(id, "DADA", name, attr_val) < 0 ) val = 0;
  else val = strtod(attr_val, NULL);
}

void get_val(hid_t id, const char *name, int& val) {
  char attr_val[256];
  if ( H5LTget_attribute_string(id, "DADA", name, attr_val) < 0 ) val = 0;
  else val = atoi(attr_val);
}

void get_val(hid_t id, const char *name, long long int& val) {
  char attr_val[256];
  if ( H5LTget_attribute_string(id, "DADA", name, attr_val) < 0 ) val = 0;
  else val = atoll(attr_val);
}

void get_val(hid_t id, const char *name, string& val) {
 char attr_val[256];
 if ( H5LTget_attribute_string(id, "DADA", name, attr_val) < 0 ) val = "";
  else val = attr_val;
}

void LedaH5Data::load_correlator_dump(int dump_index, int subband_index) {
  hid_t file_id, dataset_id, space_id, memspace_id, dataspace_id;
  int ndims;
  hsize_t *dims;
  int num_channels, num_baselines, num_polarizations;

  if ( dump_index < 0 || n_dumps <= dump_index ) {
    cout << "Invalid dump_index " << dump_index << " passed to load_correlator_dump\n";
    exit(1);
  }

  if ( subband_index < 0 || n_subbands <= subband_index ) {
    cout << "Invalid dump_index " << dump_index << " passed to load_correlator_dump\n";
    exit(1);
  }

  
  file_id = H5Fopen(file_name.c_str(), H5F_ACC_RDONLY, H5P_DEFAULT); 
  dataset_id = H5Dopen(file_id,"DADA",H5P_DEFAULT);

  // Find out the size by finding the dimensions
  space_id = H5Dget_space(dataset_id); 
  ndims = H5Sget_simple_extent_ndims(space_id); 
  if ( (dims = (hsize_t*)malloc(ndims*sizeof(hsize_t))) == NULL ) { 
     cerr << "Failed to malloc for ndims\n";
     exit(1);
  }
  H5Sget_simple_extent_dims(space_id,dims,NULL); 
  //for (i=0; i<ndims; ++i) printf("%lld\n",dims[i]);

  num_channels = dims[2];
  num_baselines = dims[3];
  num_polarizations = dims[4];

  hsize_t     count[ndims];             
  hsize_t     offset[ndims];             
  hsize_t     stride[ndims];
  hsize_t     block[ndims];

  for (int i=0; i<ndims; ++i) {
    stride[i] = block[i] = 1;
    offset[i] = 0;
  }

  count[0]  = 1;
  count[1]  = 1;
  count[2] = num_channels;
  count[3] = num_baselines;
  count[4] = num_polarizations;
  count[5] = dims[5];

  memspace_id = H5Screate_simple(ndims, count, NULL); 
  dataspace_id = H5Dget_space (dataset_id);

  offset[0] = dump_index;
  offset[1] = subband_index;

  if ( dump_data == NULL )
    dump_data = new float[dims[2]*dims[3]*dims[4]*dims[5]];

  H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, stride, count, block);
  H5Dread (dataset_id, H5T_NATIVE_FLOAT, memspace_id, dataspace_id, H5P_DEFAULT, dump_data);

}

complex<float> LedaH5Data::get_vis_value(int chan, int stand1, int stand2, int pol1, int pol2) {

  float f1, f2;
  int bl_index, tmp;

  // dada indexes have stand1 > stand 2
  if ( stand1 < stand2 ) {
    tmp = stand1; stand1 = stand2; stand2 = tmp;
    tmp = pol1; pol1 = pol2; pol2 = tmp;
  }
  bl_index = stand1*(stand1+1)/2+stand2;

  f1 = dump_data[( (chan*NUM_BASELINES + bl_index)*NUM_POLS+pol1*2+pol2 )*NUM_FLOATS_PER_COMPLEX];
  f2 = dump_data[( (chan*NUM_BASELINES + bl_index)*NUM_POLS+pol1*2+pol2 )*NUM_FLOATS_PER_COMPLEX+1];

  return complex<float>(f1, f2);
}


// Constructor
LedaH5Data::LedaH5Data(const char *fname) {
  hid_t file_id, space_id, dataset_id;
  int ndims;
  hsize_t *dims;
  vector<string> words;

  dump_data = NULL;

  file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT); 
  file_name = fname;

  // Dimensions
  dataset_id = H5Dopen(file_id,"DADA",H5P_DEFAULT);
  space_id = H5Dget_space(dataset_id); 
  ndims = H5Sget_simple_extent_ndims(space_id); 
  if ( (dims = new hsize_t[ndims]) == NULL ) { 
     cerr << "Failed to malloc for dims\n";
     exit(1);
  }
  H5Sget_simple_extent_dims(space_id,dims,NULL); 
  //for (int i=0; i<ndims; ++i) printf("%lld\n",dims[i]);

  //Header
  get_val(file_id, "BW", BW);
  get_val(file_id, "RA", RA);
  get_val(file_id, "DEC", DEC);
  get_val(file_id, "FREQ", FREQ);
  get_val(file_id, "HDR_SIZE", HDR_SIZE);                
  get_val(file_id, "HDR_VERSION", HDR_VERSION);
  get_val(file_id, "INSTRUMENT", INSTRUMENT);
  get_val(file_id, "MODE", MODE);
  get_val(file_id, "NBIT", NBIT);           
  get_val(file_id, "NCHAN", NCHAN);
  get_val(file_id, "NDIM", NDIM);
  get_val(file_id, "NPOL", NPOL);
  get_val(file_id, "NSTATION", NSTATION);
  get_val(file_id, "XENGINE_NTIME", XENGINE_NTIME);
  get_val(file_id, "NAVG", NAVG);                 
  get_val(file_id, "DATA_ORDER", DATA_ORDER);
  get_val(file_id, "SKY_STATE_PHASE", SKY_STATE_PHASE);
  get_val(file_id, "BYTES_PER_AVG", BYTES_PER_AVG); 
  get_val(file_id, "BYTES_PER_SECOND", BYTES_PER_SECOND);         
  get_val(file_id, "OBS_OFFSET", OBS_OFFSET);       
  get_val(file_id, "PID", PID);
  get_val(file_id, "RECEIVER", RECEIVER);
  get_val(file_id, "SOURCE", SOURCE);
  get_val(file_id, "TELESCOPE", TELESCOPE);
  get_val(file_id, "TSAMP", TSAMP);
  get_val(file_id, "PROC_FILE", PROC_FILE);
  get_val(file_id, "OBS_XFER",  OBS_XFER);
  string frequency_list;
  get_val(file_id, "CFREQ", frequency_list);

  words = split(frequency_list);
  if ( words.size() != dims[1] ) {
    cerr << "Number of frequencies listed in header, does not equal number of frequencies in the data\n";
    exit(1);
  }
  if ( (CFREQ = new double[dims[1]]) == NULL ) { 
     cerr << "Failed to malloc for frequency list\n";
     exit(1);
  }  
  for (int i=0; i<words.size(); ++i) CFREQ[i] = strtod(words[i].c_str(), NULL);
  get_val(file_id, "UTC_START", UTC_START);

  n_dumps = dims[0]; n_subbands = dims[1];

  H5Fclose(file_id);
}

LedaH5Data::~LedaH5Data() { 
  if ( dump_data != NULL ) delete[] dump_data;
}

void LedaH5Data::print_info() {
  cout << "Dimensions: " << n_dumps << "(time) x " << n_subbands << "(subband)" << endl;
  cout << "BW  " << BW << endl;
  cout << "RA  " << RA << endl;
  cout << "DEC  " << DEC << endl;
  cout << "FREQ  " << FREQ << endl;
  cout << "HDR_SIZE  " << HDR_SIZE << endl;
  cout << "HDR_VERSION  " << HDR_VERSION << endl;
  cout << "INSTRUMENT  " << INSTRUMENT << endl;
  cout << "MODE  " << MODE << endl;
  cout << "NBIT  " << NBIT << endl;
  cout << "NCHAN  " << NCHAN << endl;
  cout << "NDIM  " << NDIM << endl;
  cout << "NPOL  " << NPOL << endl;
  cout << "NSTATION  " << NSTATION << endl;
  cout << "XENGINE_NTIME  " << XENGINE_NTIME << endl;
  cout << "NAVG  " << NAVG << endl;
  cout << "DATA_ORDER  " << DATA_ORDER << endl;
  cout << "SKY_STATE_PHASE  " << SKY_STATE_PHASE << endl;
  cout << "BYTES_PER_AVG  " << BYTES_PER_AVG  << endl;
  cout << "BYTES_PER_SECOND  " << BYTES_PER_SECOND << endl;
  cout << "OBS_OFFSET  " << OBS_OFFSET << endl;
  cout << "PID  " << PID << endl;
  cout << "RECEIVER  " << RECEIVER << endl;
  cout << "SOURCE  " << SOURCE << endl;
  cout << "TELESCOPE  " << TELESCOPE << endl;
  cout << "TSAMP  " << TSAMP << endl;
  cout << "PROC_FILE  " << PROC_FILE << endl;
  cout << "OBS_XFER  " << OBS_XFER << endl;
  cout << "CFREQ ";
  for ( int i=0; i<n_subbands; ++i) cout << " " << CFREQ[i];
  cout << endl;
  cout << "UTC_START  " << UTC_START << endl;
}
