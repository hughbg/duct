/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <fstream>
#include <iostream>
#include <vector>
#include <cstdlib>
#include "header.h"
#include "globals.h"

static string padding(int i) {
  if ( i < 10 ) return string("0"); else return string("");
}

Header::Header(const char* filename) {
  ifstream file(filename);
  string line, munch;
  vector <string> words;

  n_scans=0;
  n_chans=0;
  integration_time = 0.0;
  cent_freq_MHz = 0;
  bandwidth_MHz = 0;
  ra_hrs = -99.0;
  dec_degs = -99.0;
  year=0;
  month=0;
  day=0;
  hour=0;
  minute=0;
  second=0;
  field_name="";
  invert_freq = false;    // correlators have now been fixed
  conjugate = false;      // just in case.
  geom_correct = true;   // default, stop the fringes

  if ( file.is_open() ) {
    while( getline(file, line) ) 
      if ( line.size() > 0 && line[0] != '#' ) {
	words = split(line);

        if ( words[0] =="FIELDNAME") field_name = words[1].c_str();
        if ( words[0] =="N_SCANS") n_scans = atoi(words[1].c_str());
        if ( words[0] =="N_CHANS") n_chans = atoi(words[1].c_str());
        if ( words[0] =="INT_TIME") integration_time = atof(words[1].c_str());
        if ( words[0] =="FREQCENT") cent_freq_MHz = atof(words[1].c_str());
        if ( words[0] =="BANDWIDTH") bandwidth_MHz = atof(words[1].c_str());
        if ( words[0] =="INVERT_FREQ") invert_freq = atoi(words[1].c_str());
        if ( words[0] =="CONJUGATE") conjugate = atoi(words[1].c_str());
        if ( words[0] =="GEOM_CORRECT") geom_correct = atoi(words[1].c_str());
	// HA_HRS isn't used. Everything goes from the RA.
        if ( words[0] =="RA_HRS") ra_hrs = atof(words[1].c_str());
        if ( words[0] =="DEC_DEGS") dec_degs = atof(words[1].c_str());
        if ( words[0] =="DATE") {
	    munch = ""; for (int i=0; i<4; ++i) munch += words[1][i]; year = atoi(munch.c_str());
 	    munch = ""; for (int i=4; i<6; ++i) munch += words[1][i]; month = atoi(munch.c_str());
	    munch = ""; for (int i=6; i<8; ++i) munch += words[1][i]; day = atoi(munch.c_str());
        }
        if ( words[0] =="TIME") {
	    munch = ""; for (int i=0; i<2; ++i) munch += words[1][i]; hour = atoi(munch.c_str());
	    munch = ""; for (int i=2; i<4; ++i) munch += words[1][i]; minute = atoi(munch.c_str());
	    munch = ""; for (int i=4; i<words[1].size(); ++i) munch += words[1][i]; second = atof(munch.c_str());
        }
    
      }
  }

  /* sanity checks and defaults */
  if (n_scans == 0) {
    n_scans=1;
    cerr << "ERROR: N_SCANS unspecified.\n"; exit(1);
  }
  if( field_name.size() == 0 ) field_name = "LEDA_OVRO";
  if (n_chans==0) {
    cerr << "ERROR: N_CHANS unspecified.\n"; exit(1);
  }
  if (integration_time==0) {
    cerr << "ERROR: INT_TIME unspecified\n"; exit(1);
  }
  if (bandwidth_MHz==0) {
    cerr << "ERROR: BANDWIDTH unspecified\n"; exit(1);
  }
  if (cent_freq_MHz==0) {
    cerr << "ERROR: FREQCENT unspecified.\n"; exit(1);
  }
  if (ra_hrs==-99) {
    cerr << "ERROR: RA_HRS unspecified\n"; exit(1);
  }
  if (dec_degs==-99) {
    cerr << "ERROR: DEC_DEGS unspecified.\n"; exit(1);
  }
  if (year==0 || month==0 || day==0) {
    cerr << "ERROR: DATE unspecified or y/m/d = 0.\n"; exit(1);
  }
  if (hour==0 || month==0 || day==0) {
    cerr << "ERROR: TIME unspecified or h/m/s = 0.\n"; exit(1);
  }
 

}

void Header::print() {
  cout << "FIELDNAME\t" << field_name << endl;
  cout << "N_SCANS\t\t" << n_scans << endl;
  cout << "N_CHANS\t\t" << n_chans << endl;
  cout << "INT_TIME\t" << integration_time << endl;
  cout << "FREQCENT\t" << cent_freq_MHz << endl;
  cout << "BANDWIDTH\t" << bandwidth_MHz << endl;
  cout << endl;
  cout << "RA_HRS\t\t" << ra_hrs << endl;
  cout << "DEC_DEGS\t" << dec_degs << endl;
  cout << "DATE\t\t" << year << padding(month) << month <<  padding(day) << day << endl;
  cout << "TIME\t\t" << padding(hour) << hour << padding(minute) << minute << padding(second) << second << endl;
  cout << "INVERT_FREQ\t" << invert_freq << endl;
  cout << "CONJUGATE\t" << conjugate << endl;

};


/*
void main() {
  Header header;
 
  header.read("/data1/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702/header.txt");
  header.print();
}*/

