/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <sys/types.h>
#include <sys/stat.h>

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <vector>
#include "globals.h"
#include "dada_reader.h"

static const float SCALE_FACTOR = 6.4e-06;

DadaReader::DadaReader(const char* filename, const int what_to_load, const float scale_factor) {
  char header_str[HEADER_SIZE+1];
  int n_scans;

  if ( sizeof(float) != 4 ) {
    cerr << "Float is not 4 bytes\n";
    exit(1);
  }
  if ( sizeof(complex<float>) != 8 ) {
    cerr << "complex is not 8 bytes\n";
    exit(1);
  }
  dada.open(filename, ios::in|ios::binary);
  if ( !dada.is_open() ) {
    cerr << "Failed to open dada file " << filename << endl;
    exit(1);
  }
  time_scan_loaded = -1;
  data = new complex<float>[TIME_SCAN_SIZE];
 
  // Process header
  dada.read(header_str, HEADER_SIZE);

  if ( !dada || dada.gcount() != HEADER_SIZE ) {
    cerr << "Error reading dada file header. Count " << dada.gcount() << endl;
    exit(1);
  }

  header_str[HEADER_SIZE] = '\0';
  if ( strstr(header_str, "TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX") == 0 ) {
    cerr << "File is not TRIANGULAR format\n";
    exit(1);
  }

  strcpy(header_string, header_str);
  parse_header(header_str);

  //data[HEADER_SIZE] = '\0';
  //cout << data << endl; exit(0);

  outrigger_start_index = dada_index(0,251);     // cuwarp order   stand1<=stand2

  vis_scale = scale_factor; 

  if ( what_to_load != ALL_SCANS && what_to_load != HEADER_ONLY) {			 // pretend we are only looking at one scan
    // Calculate the number of correlator dumps
    n_scans = (get_file_size(filename)-HEADER_SIZE)/TRIANGULAR_FRAME_SIZE_BYTES;
    if ( what_to_load != ALL_SCANS && (what_to_load < 0 || n_scans <= what_to_load) ) {
      cerr << "Scan number: " << what_to_load << ", no such scan\n";
      exit(1);
    }
 
    for (int i=0; i<what_to_load; ++i) { 
      cout << "Skipping dump " << i << endl;
      next_time_scan();
    }
  }

}

complex<float> DadaReader::get_vis_value(int chan, int stand1, int stand2, int pol1, int pol2) {

  complex<float> vis;
  int bl_index, tmp;

  // dada indexes have stand1 > stand 2
  if ( stand1 < stand2 ) {
    tmp = stand1; stand1 = stand2; stand2 = tmp;
    tmp = pol1; pol1 = pol2; pol2 = tmp;
  }
  bl_index = stand1*(stand1+1)/2+stand2;

  vis = data[chan*NUM_BASELINES*NUM_POLS + bl_index*NUM_POLS + pol1*2 + pol2];
  //if ( reverse ) return conj(vis);  
  //else return vis;
  return vis;
}

size_t DadaReader::get_file_size(string filename) {
  struct stat stat_buf;
  int rc = stat(filename.c_str(), &stat_buf);
  return rc == 0?stat_buf.st_size:-1;
}

DadaReader::~DadaReader() {
  dada.close();
  delete[] data;
}

void DadaReader::parse_header(char *header_str) {
  char *start, *end;
  key_value kv;  
  bool found_order;

  start = end = header_str;

  while ( *end != '\0' ) {
    while ( *end != '\n' && *end != '\0' ) ++end;

    vector<string> words = split(string(start, end-start));
    if ( words.size() != 2 ) {
      cerr << "Error: Found invalid key/value pair in DADA header at \"" << start << "\"" << endl;
      exit(1);
    }
    kv.key = words[0]; kv.value = words[1];
    header.push_back(kv);

   
    ++end; start = end; 
  }

  found_order = false;
  for (int i=0; i<header.size(); ++i) 
    if ( header[i].key == "DATA_ORDER" ) found_order = true; 
  if ( !found_order ) {
    cerr << "Error: DATA_ORDER not found in DADA file.\n";
    exit(1);
  }  
  //for (int i=0; i<header_index; ++i) cout << header[i].key << " " << header[i].value << endl;
}

bool DadaReader::in_header(const char *s) {
  for (int i=0; i<header.size(); ++i) 
    if ( header[i].key == s ) return true;
  return false;
}

string DadaReader::get_header_value(const char *s) {
  for (int i=0; i<header.size(); ++i) 
    if ( header[i].key == s ) return header[i].value;
  return string("");
}


void DadaReader::next_time_scan() {
  ++time_scan_loaded;

  dada.read((char*)data, TIME_SCAN_SIZE*sizeof(complex<float>));
  if ( !dada || dada.gcount() != TIME_SCAN_SIZE*sizeof(complex<float>) ) {
    cerr << "Error reading dada file at time scan " << time_scan_loaded << ". Read " << dada.gcount() << endl;
    exit(1);
  }

  if ( vis_scale != 1 ) {
    for (int i=0; i<TIME_SCAN_SIZE; ++i) { 
       data[i] *= vis_scale;
    }
  }
 
  // Takes too long
  //max = 0;
  //for (int i=0; i<TIME_SCAN_SIZE; ++i) 
  //  if ( abs(data[i]) > max ) max = abs(data[i]);

}

/*
int main() {
  complex<float> vis;
  DadaReader dada("/data1/hg/dada_plot/2016-02-03-22_37_50_0001287429875776.dada");
  vector<key_value> header = dada.get_header();

  for (int i=0; i<header.size(); ++i) cout << header[i].key << " " << header[i].value << endl;

  for (int i=0; i<10; ++i)  dada.next_time_scan();

  //cout << "Max vis " << dada.max << endl;
 
  vis = dada.get_outrigger_value(7, 0, 0, 0, 0); cout << vis << endl; 
  vis = dada.get_outrigger_value(7, 0, 0, 0, 1); cout << vis << endl;  
  vis = dada.get_outrigger_value(7, 0, 0, 1, 0); cout << vis << endl;  
  vis = dada.get_outrigger_value(7, 0, 0, 1, 1); cout << vis << endl;

  // should be the numbers 0-31625
  for (int stand1=0; stand1<NUM_STANDS; ++stand1) 
    for (int stand2=stand1; stand2<NUM_STANDS; ++stand2)
      if ( stand2 < NUM_INRIGGERS ) cout << "in " << stand1 << " " << stand2 << " " << dada.dada_index(stand1,stand2) << endl; 

  cout << dada.outrigger_start_index<<endl;
 
 // should be the numbers 0-1269
  //for (int stand1=0; stand1<NUM_STANDS; ++stand1) 
  //  for (int stand2=stand1; stand2<NUM_STANDS; ++stand2)
  //    if ( stand2 >= NUM_INRIGGERS ) cout << "out " << stand1 << " " << stand2 << " " << dada.outrigger_index(stand1,stand2) << endl; 

  
}*/
