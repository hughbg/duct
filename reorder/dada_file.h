#include <complex>
using namespace std;
class DadaFile {
private:
  complex<float> *data;
  
public:
  DadaFile(string fname, int num_scans);
  ~DadaFile();
  complex<float> get_value(int dump, int baseline, int chan, int pol1, int pol2);
};

extern int make_dada_index(int stand1, int stand2);
