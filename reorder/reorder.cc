#include <iostream>
#include <fstream>
#include <vector>
#include <cstdlib>
#include <sstream>
#include <fts.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "unistd.h"
#include "dada_file.h"
#include "baseline_file.h"
#include "constants.h"



vector<string> split(string line) {
   vector<string> tokens;
   string token;

   for (int i=0; i<line.size(); ++i) if ( line[i] == '\t' ) line[i] = ' ';
   stringstream l(line);

   while ( getline(l, token, ' ') )
     if ( token.size() > 0 ) tokens.push_back(token);

   return tokens;
}


static int recursive_delete(const char *dir) {
    int ret = 0;
    FTS *ftsp = NULL;
    FTSENT *curr;

    // Cast needed (in C) because fts_open() takes a "char * const *", instead
    // of a "const char * const *", which is only allowed in C++. fts_open()
    // does not modify the argument.
    char *files[] = { (char *) dir, NULL };

    // FTS_NOCHDIR  - Avoid changing cwd, which could cause unexpected behavior
    //                in multithreaded programs
    // FTS_PHYSICAL - Don't follow symlinks. Prevents deletion of files outside
    //                of the specified directory
    // FTS_XDEV     - Don't cross filesystem boundaries

    ftsp = fts_open(files, FTS_NOCHDIR | FTS_PHYSICAL | FTS_XDEV, NULL);
    if ( !ftsp ) {
      fprintf(stderr, "%s: fts_open failed\n", dir);
      return -1;
    }

    while ( (curr = fts_read(ftsp)) ) {
        switch ( curr->fts_info ) {
        case FTS_NS:
        case FTS_DNR:
        case FTS_ERR:
            fprintf(stderr, "%s: fts_read error\n",
                    curr->fts_accpath);
            break;

        case FTS_DC:
        case FTS_DOT:
        case FTS_NSOK:
            // Not reached unless FTS_LOGICAL, FTS_SEEDOT, or FTS_NOSTAT were
            // passed to fts_open()
            break;

        case FTS_D:
            // Do nothing. Need depth-first search, so directories are deleted
            // in FTS_DP
            break;

        case FTS_DP:
        case FTS_F:
        case FTS_SL:
        case FTS_SLNONE:
        case FTS_DEFAULT:
            if (remove(curr->fts_accpath) < 0) {
                fprintf(stderr, "%s: Failed to remove\n",
                        curr->fts_path);
                ret = -1;
            }
            break;
        }
    }


    if (ftsp) {
        fts_close(ftsp);
    }

    return ret;
}




static void reorder(string **dada_file_names, int num_files_in_time, int num_files_in_frequency, int num_scans_in_file) {
  int i, j, dada_file, stand, other_stand, channel, scan, baseline_index, pol1, pol2;
  DadaFile **dada_files;
  complex<float> **dada_file_data, val;
  BaselineFile *file_streams[NUM_BASELINES]; 

  for (int i=0; i<NUM_BASELINES; ++i) file_streams[i] = NULL;

  // Clear existing files
  cout << "Removing existing files\n";
  for (stand=0; stand<NUM_STANDS; ++stand) {
    stringstream stand_directory;
    stand_directory << OUTPUT_ROOT << "/" << "stand_" << stand;

    if ( access(stand_directory.str().c_str(), F_OK) != -1 ) recursive_delete(stand_directory.str().c_str());
  }
      
  cout << "Reordering\n";
  // Scan the file matrix and do work
  for (i=0; i<num_files_in_time; ++i) {

    // Load files all freq. Max is 22.
    dada_files = new DadaFile*[num_files_in_frequency];
    for (j=0; j<num_files_in_frequency; ++j) { 
      dada_files[j] = new DadaFile(dada_file_names[i][j], num_scans_in_file);
    } 

    // Extract the data. A different file for each stand gets the output.
    for (stand=0; stand<NUM_STANDS; ++stand) {
      cout << "Doing stand " << stand << endl;

      for (other_stand=0; other_stand<=stand; ++other_stand) {
        stringstream stand_file_name;

        baseline_index = make_dada_index(stand, other_stand);

	// Make the directory and file
        stand_file_name << OUTPUT_ROOT << "/" << "stand_" << stand;      
        mkdir(stand_file_name.str().c_str(), 0755);
        stand_file_name << "/stand_" << other_stand << ".dat";

        if ( file_streams[baseline_index] == NULL ) 
          if ( (file_streams[baseline_index]=new BaselineFile(stand_file_name.str())) == NULL ) {
            cerr << "Failed to allocate memory for baseline buffer " << stand << " " << other_stand << endl;
            exit(1);
          }
 
        // Levels: top level by scan
        // In a scan are 109*num_files_in_frequency channels
        // in a channeli are 4 pols
        for (scan=0; scan<num_scans_in_file; ++scan) 
          for (dada_file=0; dada_file<num_files_in_frequency; ++dada_file)
            for (channel=0; channel<NUM_CHANNELS; ++channel) 
              for (pol1=0; pol1<2; ++pol1)  
                for (pol2=0; pol2<2; ++pol2) {
		   // Ordering for a file: at top level there are blocks of scans. Within each scan, num_files_in_frequency*NUM_CHANNELS channels.
                   // Within each channel, 4 pols.
                  file_streams[baseline_index]->write(dada_files[dada_file]->get_value(scan, baseline_index, channel, pol1, pol2));
                  
                }
      }
    }
    //cout << "Done time step " << i << endl;

    for (j=0; j<num_files_in_frequency; ++j) {
      delete dada_files[j];
    } 
    delete[] dada_files;

    //for (j=0; j<NUM_BASELINES; ++j) delete file_streams[j];
    
  }

  cout << "Flushing file buffers\n";
  for (int i=0; i<NUM_BASELINES; ++i) delete file_streams[i];
  cout << "Done\n";

}

void do_reorder(const char *fname) {
  int num_files_in_time, num_files_in_frequency, num_scans_in_file;
  string **dada_files, line;
  vector<string> words;
  int i, j;
  

  // Read the file set
  ifstream file_list(fname);
  if ( !file_list.is_open() ) {
    cerr << "Can't open filelist " << fname << endl;
    exit(1);
  }

  // Initialise memory for names
  if ( !getline(file_list, line) ) {
    cerr << "Failed to read file list\n";
    exit(1);
  }
  words = split(line);
  if ( words.size() != 3 ) {
    cerr <<  "Expecting 3 numbers at start of file list " << line << endl;
    exit(1);
  }
  num_files_in_time = atoi(words[0].c_str());	// only 3 numbers used here
  num_files_in_frequency = atoi(words[1].c_str());
  num_scans_in_file = atoi(words[2].c_str());

  dada_files = new string*[num_files_in_time];
  for (i=0; i<num_files_in_time; ++i) dada_files[i] = new string[num_files_in_frequency];

  // Read the list
  for (i=0; i<num_files_in_time; ++i)  
    for (j=0; j<num_files_in_frequency; ++j) {
      if ( !getline(file_list, dada_files[i][j]) ) {
        cerr << "Failed to read file list\n";
        exit(1);
      }     
   }

  file_list.close();


  // Print the list
  /*cout << num_files_in_time << " " << num_files_in_frequency << " " << num_scans_in_file << endl;
  for (i=0; i<num_files_in_time; ++i)  
   for (j=0; j<num_files_in_frequency; ++j) 
     cout << dada_files[i][j] << endl;
  */

 
  reorder(dada_files, num_files_in_time, num_files_in_frequency, num_scans_in_file);

  
}

int main(int argc,char *argv[]) {
  if ( argc != 2 ) {
    cerr << "Need one file name as argument i.e. the file list\n";
    exit(1);
  }
  do_reorder(argv[1]);
}
