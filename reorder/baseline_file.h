/*
  This is just a write-out buffer
*/

#include <complex>
#include <string>
#include <fstream>
using namespace std;

class BaselineFile {
private:
  complex<float> *buffer;
  int buffer_length, buffer_top;
  int default_length;
  ofstream baseline_file_out;
  ifstream baseline_file_in;
  string file_name;
  int num_written;

  void flush_buffer();
  void ensure_buffer(int length);

public:

  BaselineFile(string fname);
  void write(complex<float> vis);
  void load();
  complex<float> get(int num_channels, int scan, int channel, int pol);
  ~BaselineFile();
};
  
