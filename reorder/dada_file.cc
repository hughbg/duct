#include <iostream>
#include <fstream>
#include <cstdlib>
#include "dada_file.h"
#include "constants.h"


int make_dada_index(int stand1, int stand2) {
  
  if ( stand1 <= stand2 ) return stand2*(stand2+1)/2+stand1;
  else return stand1*(stand1+1)/2+stand2;
}


DadaFile::DadaFile(string fname, int num_scans) {

  cout << "Loading " << fname << endl;

  if ( (data = new complex<float>[TIME_SCAN_LENGTH*num_scans]) == NULL ) {
    cerr << "Failed getting memory to load DADA file " << fname << endl;
    exit(1);
  }

  ifstream input_file(fname.c_str(), ios::in|ios::binary);
  if ( !input_file.is_open() ) {
    cerr << "Failed to open " << fname << endl;
    exit(1);
  }


  input_file.read((char*)data, HEADER_SIZE);
  if ( !input_file || input_file.gcount() != HEADER_SIZE ) {
    cerr << "Error reading dada file header. Count " << input_file.gcount() << endl;
    exit(1);
  }
  input_file.read((char*)data, TIME_SCAN_LENGTH*num_scans*sizeof(complex<float>));
  if ( !input_file || input_file.gcount() !=  TIME_SCAN_LENGTH*num_scans*sizeof(complex<float>) ) {
    cerr << "Error reading dada file. Count " << input_file.gcount() << endl;
    exit(1);
  }
  input_file.close();

}

DadaFile::~DadaFile() { 
  if ( data != NULL ) delete[] data; 
}

complex<float> DadaFile::get_value(int dump, int baseline, int chan, int pol1, int pol2) {
  return data[TIME_SCAN_LENGTH*dump+chan*NUM_BASELINES*NUM_POLS + baseline*NUM_POLS + pol1*2 + pol2]; 
}

/*
int main() {
  DadaFile dada("/nfs/longterm/ledaovro7/data1/one/2015-04-08-20_15_03_0001133593833216.dada", 10);
  cout << dada.get_value(3,702,0,1,0) << endl;
  cout << dada.get_value(3,702,1,1,0) << endl;
  cout << dada.get_value(3,702,2,1,0) << endl;
  cout << dada.get_value(3,702,3,1,0) << endl;
  cout << dada.get_value(3,702,4,1,0) << endl;
  cout << dada.get_value(3,702,5,1,0) << endl;
  cout << dada.get_value(3,702,6,1,0) << endl;
  cout << dada.get_value(3,702,7,1,0) << endl;
  cout << dada.get_value(3,702,8,1,0) << endl;
  cout << dada.get_value(3,702,9,1,0) << endl;

}
*/
