#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cstdlib>
#include "baseline_file.h"
#include "constants.h"


#define BUF_SIZE (5120)       // must be divisible by 8

BaselineFile::BaselineFile(string fname) {
  file_name = fname;
  buffer_length = buffer_top = 0; num_written = 0;
  buffer = NULL;
  default_length = BUF_SIZE/sizeof(complex<float>);
}

BaselineFile::~BaselineFile() {
  if ( buffer != NULL ) {
    if ( buffer_top > 0 ) flush_buffer();
    delete[] buffer;
  }
  //cout << "Num written " << num_written << endl;
}



void BaselineFile::ensure_buffer(int length) {
  if ( buffer == NULL || buffer_length != length ) {
    delete[] buffer; 
    buffer_length = length;
    if ( (buffer=new complex<float>[buffer_length]) == NULL ) {
      cerr << "Failed getting memory for baseline file buffer " << file_name << endl;
      exit(1);
    }
    buffer_top = 0;
  }
}   


void BaselineFile::flush_buffer() {
  baseline_file_out.open(file_name.c_str(), ios::out|ios::binary|ios::app);
  if ( !baseline_file_out.is_open() ) {
    cerr << "Failed opening baseline file " << file_name << endl;
    exit(1);
  }
  baseline_file_out.write((const char*)buffer, buffer_top*sizeof(complex<float>));
  if ( !baseline_file_out ) {
    cerr << "Failed to write baseline file " << file_name << endl;
    cerr << "Stopping.\n"; exit(1);
  }
  baseline_file_out.close();
  delete[] buffer;
  num_written += buffer_length*sizeof(complex<float>);
  buffer = NULL; buffer_length = 0;
  buffer_top = 0;
}

void BaselineFile::write(complex<float> vis) {
  ensure_buffer(default_length);   // might not be allocated
  buffer[buffer_top++] = vis;

  if ( buffer_top >= buffer_length ) flush_buffer();
}

void BaselineFile::load() {
  struct stat sb;
 
  if ( stat(file_name.c_str(), &sb) == -1 ) {
    cerr << "Failed stat on baseline file " << file_name << endl;
    exit(1);
  }
  ensure_buffer(sb.st_size/sizeof(complex<float>));

  baseline_file_in.open(file_name.c_str(), ios::in|ios::binary);
  if ( !baseline_file_in.is_open() ) {
    cerr << "Failed opening baseline file " << file_name << endl;
    exit(1);
  }
  baseline_file_in.read((char*)buffer, buffer_length*sizeof(complex<float>));
  baseline_file_in.close(); 
}

complex<float> BaselineFile::get(int num_all_channels, int scan, int channel, int pol) {
  int num_vals_per_scan = num_all_channels*NUM_POLS;
  return buffer[scan*num_vals_per_scan+channel*NUM_POLS+pol];
}

/*
int main() {
  BaselineFile bf("x.bin",109);		// writing - make sure doesn't exist
  cout << sizeof(complex<float>) << endl;   // this better be 8
  complex<float> c(0,0);		
  bf.add(c);
  bf.add(c);
  bf.add(c);
  bf.add(c);    // this better have written 32
}
*/
