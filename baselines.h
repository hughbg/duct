/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef BASELINES_H
#define BASELINES_H

/*
 Routines for baseline indexing and baseline coding. Codes will be put into an array. 

*/



#include "globals.h"

class Baselines {
public:
  int indexes[NUM_STANDS][NUM_STANDS];
  float *codes;

  Baselines();

  // Generate the MIRIAD code for a baseline from the two stand numbers.
  // The code is not the same as a baseline index. A baseline index
  // is the index into a list of codes (and other things). 
  int encodeBaseline(int stand1_index, int stand2_index);
  void decodeBaseline(int code, int *ant1, int *ant2);

  // Index used for a  lists,  like UVW
  int baseline_index(int stand1_index, int stand2_index);

};

#endif


