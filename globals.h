/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GLOBALS_H
#define GLOBALS_H

#include <cmath>
#include <vector>
#include <string>
using namespace std;

const int VLIGHT = 299792458.0;        // speed of light. m/s

// LEDA location
// These come from Ben's JSON file and were set after the error in the northing was discovered.
// https://docs.google.com/spreadsheets/d/1HrX8buuUgGpVLaJy-96sF9-xXH_YJ36Bycp8IWdcBAs/edit?pref=2&pli=1#gid=302694866
const double LONGITUDE = -118.2816819;	// degrees
const double LATITUDE = 37.2397808;	// degrees
const double ALTITUDE = 1183.4839;	// meters

const double ARR_LAT_RAD = LATITUDE*(M_PI/180.0);	// radians
const double ARR_LON_RAD = LONGITUDE*(M_PI/180.0);	// "   "
const double HEIGHT = ALTITUDE;

const double E_SQUARED = 6.69437999014e-3;		// e^2
const double EARTH_RAD_WGS84 = 6378137.0;		// meters

/* Currently a lot of numbers are hardwired, because I don't know what to trust. Do we trust the 
   DADA header, the values generated from it, the files supplied that define things. There
   are multiple definitions, e.g. you could get the number of stands from several different files.
   The policy now is: I only trust the numbers here. Check the rest complies with them. */
const int NUM_CHANS = 109;
const int NUM_STANDS = 256;
const int NUM_BASELINES = (NUM_STANDS*(NUM_STANDS+1))/2;
const int POL_TYPE = -5;
const int NUM_X_Y_POL = 2;
const int NUM_POLS = NUM_X_Y_POL*NUM_X_Y_POL;
const int NUM_OUTRIGGER_BASELINES = (252+253+254+255+256);
const int NUM_SKY_STATES = 9;
const int NUM_OUTRIGGERS = 5;
const int NUM_INRIGGERS = NUM_STANDS-NUM_OUTRIGGERS;

const int HEADER_SIZE = 4096;        // in units of char
const int FULL_SIZE = NUM_CHANS*NUM_BASELINES*NUM_POLS;
const int OUTRIGGER_SIZE = NUM_SKY_STATES*NUM_CHANS*NUM_OUTRIGGER_BASELINES*NUM_POLS; 
const int TIME_SCAN_SIZE = FULL_SIZE+OUTRIGGER_SIZE;;  // in units of "a number", either complex or float32

enum standActions { noTransform, doXYZ, doPrecess };
enum cableActions { noCableDelay, doCableDelay };
enum lockActions { noLockPointing, doLockPointing };

// Utilities
extern vector<string> split(string line);
extern void mat_transpose(double rmat1[3][3], double rmat2[3][3]);
extern int get_num_at_end(string s);

#endif
