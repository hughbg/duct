/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
  Show how to use the C API. Just call "import_telescope_observation".
*/

#include <stdio.h>
#include "duct.h"
#include "uvfits.h"       // get uvfits.h from cuwarp

static void check_andfree(void *p) {
  if ( p != NULL ) free(p);
}

int main() {
  uvdata *data;
  int i;
  
  import_telescope_observation(&data, "/data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/header.txt", "/data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/antenna_locations.txt", "/data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/instr_config.txt", "/data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/2015-04-08-20_15_03_0001133593833216.dada",ALL_SCANS, 1);
  print(data); 

  // Check that cuwarp can free the data
  check_andfree(data->date);
  check_andfree(data->n_baselines);
  check_andfree(data->source);
  check_andfree(data->array->antennas);
  check_andfree(data->array);
  for (i=0; i<data->n_vis; ++i) {
    if ( data->u != NULL ) check_andfree(data->u[i]); if ( data->v != NULL )  check_andfree(data->v[i]); if ( data->v != NULL )  check_andfree(data->w[i]);
    if ( data->visdata != NULL )  check_andfree(data->visdata[i]); if ( data->weightdata != NULL ) check_andfree(data->weightdata[i]);
    if ( data->baseline != NULL )  check_andfree(data->baseline[i]);
  }
  check_andfree(data->u); check_andfree(data->v); check_andfree(data->w);
  check_andfree(data->visdata); check_andfree(data->weightdata);
  check_andfree(data->baseline);
  check_andfree(data);

}
  
