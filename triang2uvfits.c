/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
  Convert TRIANGULAR DADA format to UVfits.

  triang2uvfits /data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/header.txt /data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/antenna_locations.txt /data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/instr_config.txt /data2/hg/interfits/lconverter/WholeSkyL64_47.004_d20150203_utc181702_test_p/2015-04-08-20_15_03_0001133593833216.dada x.uvfits
*/

#include <stdio.h>
#include <unistd.h>
#include "duct.h"
// get uvfits.h and uvfits.c from cuwarp. Convert SLA calls to PAL. Change "station" to stand. Make writeUVFITS extern "C"
      

extern "C" int writeUVFITS(char *filename, uvdata *data);

static void usage() {
  fprintf(stderr,"Usage: triang2uvfits <header file> <ant locations file> <instr config file> <dada file> <output uvfits>\n");
  
}

int main(int argc, char *argv[]) {
  uvdata *data;
  int i;

  if ( argc != 6 ) { usage(); return 1; }

  for (i=1; i<5; ++i) {
    //puts(argv[i]);
    if ( access(argv[i],R_OK) == -1 ) {
      fprintf(stderr,"Failed to find %s\n",argv[i]);
      return 1;
    }
  }
  
  import_telescope_observation(&data, argv[1], argv[2], argv[3], argv[4], ALL_SCANS, 1);
  data->source->ra /= 15; 

  writeUVFITS(argv[5], data);

}
  
