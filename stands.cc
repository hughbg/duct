/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <fstream>
#include <cmath>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include <iomanip>
#include <set>
#include <pal.h>
#include "stands.h"
#include "globals.h"


// Class Stand ------------------------------------------------------


void Stand::print() {
  cout.precision(10);
  cout << stand_num << "\t" << east_x << "\t" << north_y << "\t" << height_z << endl;
  
}

// Class Stands ------------------------------------------------------


/******************************
 read the stand locations from a text
 file and populate the stand positions in the
 data structure.
*******************************/
Stands::Stands(const char* filename) {
  ifstream file(filename);
  string line;
  vector <string> words;
  set <int> stands_present;

  if ( file.is_open() ) {
    while( getline(file, line) ) 
      if ( line.size() > 0 && line[0] != '#' ) {
	words = split(line); 
        if ( words.size() != 4 ) {
          cerr <<  "Stands file invalid at line " << line << endl;
          exit(1);
        }

	Stand stand(words[0], atof(words[1].c_str()), atof(words[2].c_str()), atof(words[3].c_str()), get_num_at_end(words[0]));

	if ( stands_present.count(stand.stand_num) != 0 ) {
    	  cerr <<  "Stand " << NUM_STANDS << " appears multiple times in antenna file\n";
    	  exit(1);
  	}
  	if ( stand.stand_num < 0 || NUM_STANDS <= stand.stand_num ) {
          cerr <<  "Expecting " << NUM_STANDS << " stands in antenna file\n";
          exit(1);
        }
 
        list.push_back(stand);

	stands_present.insert(stand.stand_num);
      }
  } else { 
    cerr <<  "Failed to open antenna file " << filename << endl;
    exit(1);
  }

  if ( list.size() != NUM_STANDS ) {
    cerr <<  "Expecting " << NUM_STANDS << " stands in antenna file\n";
    exit(1);
  }

  // check
  for (int i=0; i<NUM_STANDS; ++i) 
    if ( stands_present.count(i) == 0 ) {
      cerr <<  "Missing stand " << i << " in antenna file\n";
      exit(1);
    }

  Geodetic2XYZ(ARR_LAT_RAD, ARR_LON_RAD, HEIGHT, &X, &Y, &Z);

}

Stands::Stands(Stands& other) {
  for (int i=0; i<other.list.size(); ++i) list.push_back(other.list[i]);
  X = other.X; Y = other.Y; Z = other.Z;
}

/***************************
  convert Geodetic lat/lon/height to XYZ coords
  uses constants from WGS84 system, defined in header
***************************/
void Stands::Geodetic2XYZ(double lat_rad, double lon_rad, double height_meters, double *X, double *Y, double *Z) {
    double s_lat,s_lon,c_lat,c_lon;
    double chi;

    s_lat = sin(lat_rad); c_lat = cos(lat_rad);
    s_lon = sin(lon_rad); c_lon = cos(lon_rad);
    chi = sqrt(1.0 - E_SQUARED*s_lat*s_lat);

    *X = (EARTH_RAD_WGS84/chi + height_meters)*c_lat*c_lon;
    *Y = (EARTH_RAD_WGS84/chi + height_meters)*c_lat*s_lon;
    *Z = (EARTH_RAD_WGS84*(1.0-E_SQUARED)/chi + height_meters)*s_lat;
}


void Stands::transform(double obs_date, double ra_hrs, double dec_degs, standActions to_do) {
  double mjd, lmst, ra_aber, dec_aber, x, y, z, xprec, yprec, zprec, ha2000, lmst2000, newarrlat;
  double rmattr[3][3], rmatpr[3][3];

  if ( to_do == doPrecess ) {
    
    mjd = obs_date-2400000.5;  // get Modified Julian date of scan.

    lmst = palDranrm(palGmst(mjd)+ARR_LON_RAD);  // local mean sidereal time, given array location

    /* Compute the apparent direction of the phase center in the J2000 coordinate system */
    aber_radec_rad(2000.0, mjd, ra_hrs*(M_PI/12.0), dec_degs*(M_PI/180.0), &ra_aber, &dec_aber);

    palPrenut(2000.0, mjd, rmattr);
    mat_transpose(rmattr, rmatpr);

    /* rmatpr undoes precession and nutation corrections */				
    ha_dec_j2000(rmatpr, lmst, ARR_LAT_RAD, ra_aber, dec_aber, &ha2000, &newarrlat, &lmst2000);
  }
  
  /* calc u,v,w at phase center and reference for all antennas relative to center of array */
  for(int i=0; i<list.size(); i++) {
    x = list[i].east_x;
    y = list[i].north_y;
    z = list[i].height_z;

        /* value of lmst at current epoch - will be changed to effective value in J2000 system 
         * 
         * To do this, need to precess "ra, dec" (in quotes on purpose) of array center
         * from value at current epoch 
         */
    if ( to_do == doXYZ || to_do == doPrecess ) ENH2XYZ_local(ARR_LAT_RAD, &x, &y, &z); 
    if ( to_do == doPrecess ) {
      precXYZ(rmatpr,x,y,z,lmst,&xprec,&yprec,&zprec,lmst2000);
      calcUVW(ha2000,dec_aber,xprec,yprec,zprec,&x,&y,&z);
    }
    list[i].east_x = x;
    list[i].north_y = y;
    list[i].height_z = z;
  }
}

Stands& Stands::operator=(Stands& other) { 
  list.clear();
  for (int i=0; i<other.list.size(); ++i) list.push_back(other.list[i]);
}



/*********************************
  convert coords in local topocentric East, North, Height units to
  'local' XYZ units. Local means Z point north, X points through the equator from the geocenter
  along the local meridian and Y is East.
  This is like the absolute system except that zero lon is now
  the local meridian rather than prime meridian.
  Latitude is geodetic, in radian.
  This is what you want for constructing the local stand positions in a UVFITS antenna table.
**********************************/
void Stands::ENH2XYZ_local(double lat, double *x, double *y, double *z) {
  double sl, cl;
  double E, N, H;

  E = *x; N = *y; H = *z;
  sl = sin(lat);
  cl = cos(lat);
  *x = -N*sl + H*cl;
  *y = E;
  *z = N*cl + H*sl;
}
 


/**************************
***************************/
/* lmst, lmst2000 are the local mean sidereal times in radians
 * for the obs. and J2000 epochs.
 */
void Stands::precXYZ(double rmat[3][3], double x, double y, double z, double lmst,
         double *xp, double *yp, double *zp, double lmst2000)
{
  double sep, cep, s2000, c2000;
  double xpr, ypr, zpr, xpr2, ypr2, zpr2;

  sep = sin(lmst);
  cep = cos(lmst);
  s2000 = sin(lmst2000);
  c2000 = cos(lmst2000);

  /* rotate to frame with x axis at zero RA */
  xpr = cep*x - sep*y;
  ypr = sep*x + cep*y;
  zpr = z;

  xpr2 = (rmat[0][0])*xpr + (rmat[0][1])*ypr + (rmat[0][2])*zpr;
  ypr2 = (rmat[1][0])*xpr + (rmat[1][1])*ypr + (rmat[1][2])*zpr;
  zpr2 = (rmat[2][0])*xpr + (rmat[2][1])*ypr + (rmat[2][2])*zpr;

  /* rotate back to frame with xp pointing out at lmst2000 */
  *xp = c2000*xpr2 + s2000*ypr2;
  *yp = -s2000*xpr2 + c2000*ypr2;
  *zp = zpr2;
}

/**************************
***************************/
/* rmat = 3x3 rotation matrix for going from one to another epoch
 * ra1, dec1, ra2, dec2 are in radians
 */

void Stands::rotate_radec(double rmat[3][3], double ra1, double dec1, double *ra2, double *dec2)
{
   double v1[3], v2[3];

  palDcs2c(ra1,dec1,v1);
  palDmxv(rmat,v1,v2);
  palDcc2s(v2,ra2,dec2);
  *ra2 = palDranrm(*ra2);
}


/**************************
***************************/
/* ra, dec, lmst units = radians */

void Stands::ha_dec_j2000(double rmat[3][3], double lmst, double lat_rad, double ra2000,
                  double dec2000, double *newha, double *newlat, double *newlmst)
{
  double nwlmst, nwlat;

  rotate_radec(rmat, lmst, lat_rad, &nwlmst, &nwlat);
  *newlmst = nwlmst;
  *newha = palDranrm(nwlmst - ra2000);
  *newlat = nwlat;
}

/**************************
***************************/
/* eq = epoch of equinox to be used (e.g., 2000.0 for J2000)
 * mjd = Modified Julian Date (TDB) of correction
 *  will ignore MJD(UTC) vs. MJD(TDB) difference here
 * v1[3] = vector in barycenter frame
 * v2[3] = corresponding vector in Earth-centered frame
 *       = apparent direction from Earth
 */

void Stands::stelaber(double eq, double mjd, double v1[3], double v2[3])
{
   double amprms[21], v1n[3], v2un[3], w, ab1, abv[3], p1dv;
   int i;

   palMappa(eq,mjd,amprms);


/* code from mapqk.c (w/ a few names changed): */

/* Unpack scalar and vector parameters */
   ab1 = amprms[11];
   for ( i = 0; i < 3; i++ )
   {
      abv[i] = amprms[i+8];
   }

   palDvn ( v1, v1n, &w );

/* Aberration (normalization omitted) */
   p1dv = palDvdv ( v1n, abv );
   w = 1.0 + p1dv / ( ab1 + 1.0 );
   for ( i = 0; i < 3; i++ ) {
      v2un[i] = ab1 * v1n[i] + w * abv[i];
   }

/* normalize  (not in mapqk.c */
   palDvn ( v2un, v2, &w );

}

/**************************
***************************/
/* ra, dec are in radians in this function call
 */

void Stands::aber_radec_rad(double eq, double mjd, double ra1, double dec1, double *ra2, double *dec2)
{
  double v1[3], v2[3];

  palDcs2c(ra1,dec1,v1);
  stelaber(eq,mjd,v1,v2);
  palDcc2s(v2,ra2,dec2);
  *ra2 = palDranrm(*ra2);
}

void Stands::calcUVW(double ha,double dec,double x,double y,double z,double *u,double *v,double *w) {
    double sh,ch,sd,cd;

    sh = sin(ha); sd = sin(dec);
    ch = cos(ha); cd = cos(dec);
    *u  = sh*x + ch*y;
    *v  = -sd*ch*x + sd*sh*y + cd*z;
    *w  = cd*ch*x  - cd*sh*y + sd*z;
}


/*
void main() {
  ant_table list;
 
  list.read("antenna_locations.tpl",0.642281);
  list.print();
}
*/

