/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
 * Loads the antennas with cable layout, cable delays, and flags.
   Usually data comes from instr_config.txt.
*/

#ifndef CABLES_H
#define CABLES_H
#include <vector>
#include <string>
#include "globals.h"

using namespace std;

class Cable {
private: 
  int m_stand_num;		// 0 based
  float m_cable_length;		// electric length in meters if coded as EL_
				// no velocity factor needs to be applied (see corr2uvfits.c)
  int m_pol_index;
  int m_flag;



public:
  Cable(int stand_num, float cable_length, int pol_index, int flag) 
    { m_stand_num = stand_num; m_cable_length = cable_length; m_pol_index = pol_index; m_flag = flag; }
  int decodePolChar(int pol_char);
  float extract_cable_len(string);

  // Getters
  int stand_num() { return m_stand_num; }
  float cable_length() { return m_cable_length; }
  int pol_index() { return m_pol_index; }
  int flag() { return m_flag; }
};


class Cables {
private:
  vector<Cable> list;
  float fast_length_table[NUM_STANDS*2];
  int fast_flag_table[NUM_STANDS*2];

public:
  
  Cables(const char *filename);
  float find_cable_length(int stand_num, int pol) { return fast_length_table[stand_num*2+pol]; }
  int find_cable_flag(int stand_num, int pol) { return fast_flag_table[stand_num*2+pol]; }
  void print();
  Cable& operator[](int i) { return list[i]; }
  int length() { return list.size(); }
};
#endif
