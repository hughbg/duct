/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef STANDS_H
#define STANDS_H

/*
 Load stand locations and optionally perform J2000 precession on them.
 Usually data comes from antenna_locations.txt.
*/


#include <vector>
#include <string>
#include "header.h"
#include "globals.h"

using namespace std;

// One stand, has a location and number
class Stand {
public:
  string name;
  double east_x, north_y, height_z;			/* position relative to array centre, meters  */
  int  stand_num;

public:
  void print();

  Stand(string n, double x, double y, double z, int num) { name = n; east_x = x; north_y = y; height_z = z; stand_num = num; }
};


// Collection of all stands
class Stands {
private:
  vector<Stand> list;

public:
  double X, Y, Z;		// centre of the array, ITRF

  Stands(const char* filename);
  Stands(Stands& other);

  Stand& operator[](int i) { return list[i]; }

  // The assignment operator makes it easy to keep copies of the stands in various
  // transformed states.
  Stands& operator=(Stands& other);

  // Calculate X, Y, Z
  void Geodetic2XYZ(double lat_rad, double lon_rad, double height_meters, double *X, double *Y, double *Z);

  // Precession functions ----

  // Top level call. Transform the stand locations. The last param specifies how to transform:
  // doXYZ: Convert east, north, height to x, y, z  (needed for cuwarp)
  // doPrecess: Execute doXYZ, and precess locations to J2000.
  //
  // The routine doesn't keep track of what state the stand locations are currently in,
  // that's up to the programmer.
  //
  // obs_date is needed because the precession will be slightly different for different times (scans) in a 
  // single DADA file, and new UVW will usually be needed for each scan. 
  void transform(double obs_date, double ra_hrs, double dec_degs, standActions to_do);  

  // Ancilliary functions for precession
  void ENH2XYZ_local(double lat, double *x, double *y, double *z);   // This one does the doXYZ action
  void precXYZ(double rmat[3][3], double x, double y, double z, double lmst,
         double *xp, double *yp, double *zp, double lmst2000);
  void rotate_radec(double rmat[3][3], double ra1, double dec1, double *ra2, double *dec2);
  void ha_dec_j2000(double rmat[3][3], double lmst, double lat_rad, double ra2000,
                  double dec2000, double *newha, double *newlat, double *newlmst);
  void stelaber(double eq, double mjd, double v1[3], double v2[3]);
  void aber_radec_rad(double eq, double mjd, double ra1, double dec1, double *ra2, double *dec2);
  void calcUVW(double ha,double dec,double x,double y,double z,double *u,double *v,double *w);


  int size() { return list.size(); }
  void print() { for (int i=0; i<list.size(); ++i) list[i].print(); }
};
#endif
