FILES=stands.cc  header.cc  cables.cc  uvdata.cc globals.cc baselines.cc dada_reader.cc duct.cc flags.cc
HEADERS=stands.h header.h cables.h uvdata.h globals.h baselines.h dada_reader.h duct.h flags.h
EXTRAS=star/strlcat.o star/star_strlcat.o star/strlcpy.o star/star_strlcpy.o star/star_strellcpy.o # These come from starlink and BSD
FLAG=-O3
SOFA_OBJ_DIR=/data1/hg/sofa/20150209/c/src/
PAL_OBJ_DIR=/data1/hg/pal-0.5.0/

all: duct tester triang2uvfits triang2regtile

duct: libduct.a main.cc
	g++ ${FLAG} -o duct main.cc -I${HOME}/include -L. -lduct -L${HOME}/lib  -lpal -lm -lduct 

tester: pretend_cuwarp.c libduct.a
	g++ -g -o tester pretend_cuwarp.c  -L. -lduct -lstdc++ -lm -L${HOME}/lib -lpal -lduct

duct_light: duct_light.cc duct_light.h libduct.a
	g++ -g -o duct_light -DTEST duct_light.cc  -L. -lduct -lstdc++ -lm -L${HOME}/lib -lpal -lduct

libduct.a: ${FILES} ${HEADERS} ${EXTRAS}
	g++ -c ${FLAG} ${FILES} -I${HOME}/include 
	ar rsc libduct.a *.o star/*.o

triang2regtile: triang2regtile.cc dada_reader.cc dada_reader.h globals.cc globals.h
	g++ ${FLAG} triang2regtile.cc -o triang2regtile dada_reader.cc globals.cc

triang2uvfits: triang2uvfits.c libduct.a uvfits.o
	g++ ${FLAG} -o triang2uvfits triang2uvfits.c uvfits.o -I${HOME}/include -L. -lduct -lstdc++ -lm -L${HOME}/lib -lpal -lduct -lcfitsio

triang2hdf5: triang2hdf5.cc dada_reader.cc globals.cc
	g++ ${FLAG} -Wunused -DCOMPRESS triang2hdf5.cc dada_reader.cc globals.cc -o triang2hdf5 -lhdf5 -lhdf5_hl

ledah5data.o:	ledah5data.cc ledah5data.h
	g++ -c ledah5data.cc

testh5: ledah5data.o testh5.cc
	g++ testh5.cc ledah5data.o globals.cc -o testh5 -lhdf5 -lhdf5_hl

sharedlib: ${FILES} ${EXTRAS}
	g++ -shared -Wl,-soname,libduct.so.1 -o libduct.so.1 -fPIC ${FILES} duct_light.cc ${EXTRAS} ${SOFA_OBJ_DIR}/*.o ${PAL_OBJ_DIR}/*.o -I${HOME}/include -L. -lduct -L${HOME}/lib  -lpal -lm -lduct 

%.o: %.c 
	gcc -c  $< -o $@ -I${HOME}/include

clean:
	rm *.o tester star/*.o duct libduct.a triang2uvfits triang2regtile

shared:
	 
