/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
  Create visibilities and uvw. Visibilities involves adding cable delay. 
  UVWs are generated from stand locations which may or may not
  have been precessed (either can be done). 
*/

#ifndef UVWDATA_H
#define UVWDATA_H
#include <string.h>
#include <complex>
#include <cstdlib>
#include "header.h"
#include "cables.h"
#include "baselines.h"
#include "stands.h"
#include "globals.h"

using namespace std;


class UVWdata {
public:
  string name;
  int n_scans;
  int n_chans;
  double cent_freq_Hz;
  float channel_width_Hz;
  double ra_hrs, dec_degs;

  double *dates;

  complex<float> **visdata;     /* array the size of n_vis whose elements point to arrays of visibiliites
                           	  the size of n_freq*n_pol*n_baselines complex floats */
  float **weightdata;  		/* weight data for visibiliites. Same data ordering as above, just float,
                           		not complex */
  double **u;           	/* arry the size of n_vis whose elements point to arrays of uvw
                           	data the size of n_baselines. Units: meters divided by speed of light */
  double **v;
  double **w;

  int true_scan;		/* if one_scan_only */

  Baselines baselines;

public:

  UVWdata(Header& header, lockActions whatLock, const int one_scan_only = -1);
  ~UVWdata();

  // Calculate UVW. This requires stand locations, which are in a Stands object.
  // The stand locations can be transformed (see Stands) in a couple of ways before
  // being subtracted to get UVWs. Lock pointing can be used, which locks the time of each
  // scan to the time of the first scan, thus producing UVWs that don't change from scan
  // to scan.
  void make_uvw(Cables& cables, Stands& stands, const standActions whatTransform, const lockActions locking);

  // Load the visibilities and transform them. Unlike corr2uvfits, no baseline reversal is needed here.
  // However, the following are needed:
  // - Conjugate all the visibilities if this is specified in the header
  // - Add cable delay, if this is specified in the "to_do" parameter
  // - Include flags (from instr_config file) by setting negative weight
  // uvw should be calculated for cable delay to be calculated, as w is needed. Does not
  // do any RFI calculation and flagging.
  void load_visibilities(const char *filename, Cables& cables, Header& header, const cableActions to_do, const float scale_factor = 1);

  // Once visibilities are loaded, extract one based on indexing
  complex<float> get_vis(const int time, const int baseline, const int chan, const int pol1, const int pol2);

  double raDeg() { return ra_hrs*15; }
  double date(const int i) { return dates[i]; }
  double raHrs() { return ra_hrs; }
  double decDeg() { return dec_degs; }
  int codePols(const int pol1, const int pol2);
  void print(const int scan);

};
#endif
