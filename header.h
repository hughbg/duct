/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
 Load telescope observation metadata. The metadata is passed to other classes
 so they can make operations like generate uvw. Usually data comes from
 header.txt.
*/

#ifndef HEADER_H
#define HEADER_H
#include <string>
using namespace std;

class Header {
public:
  int		n_scans;                // the number of time samples
  int	        n_chans;                // the number of spectral channels
  bool   	invert_freq;            // flag to indicate that freq decreases with increasing channel number.
  bool     	conjugate;              // conjugate the final vis to correct for any sign errors
  bool     	geom_correct;           // apply geometric phase correction
  float   	integration_time;       // per time sample, in seconds
  double  	cent_freq_MHz, bandwidth_MHz;   // observing central frequency and bandwidth (MHz)
  double  	ra_hrs,dec_degs;        // ra,dec of phase centre.
  int     	year,month,day;         // date/time in UTC.
  int     	hour, minute;
  float   	second;
  string    	field_name;

  Header(const char *filename);
  void print();
};
#endif
