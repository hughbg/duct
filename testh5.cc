#include "ledah5data.h"
#include <iostream>
using namespace std;

int main() {
  LedaH5Data vis_data("/data1/hg/file.h5");
  vis_data.print_info();
  vis_data.load_correlator_dump(4, 0);			    // scan index, subband index, loaded into cache
  cout << "Test vis " << vis_data.get_vis_value(45, 15, 12, 1, 1) << endl;     // channel, stand1, stand2, pol1, pol2
}
