#include <limits.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <hdf5.h>
#include <hdf5_hl.h>
#include <ctime>
#include <cstring>
#include <iostream>
#include <complex>
#include <unistd.h>
#include "globals.h"
#include "dada_reader.h"

#define PROGRAM_VERSION "1.01"
#define COMPRESSION_ID 32004

#define NUM_NUMS_PER_COMPLEX 2

// For use in converting a correlator dump to integers
static int int_mem_space[NUM_CHANS*(NUM_BASELINES+NUM_SKY_STATES*NUM_OUTRIGGER_BASELINES)*NUM_POLS*NUM_NUMS_PER_COMPLEX];


// Only used for testing
static complex<float> get_vis_value(int *data, int chan, int stand1, int stand2, int pol1, int pol2) {

  int f1, f2;
  int bl_index, tmp;

  // dada indexes have stand1 > stand 2
  if ( stand1 < stand2 ) {
    tmp = stand1; stand1 = stand2; stand2 = tmp;
    tmp = pol1; pol1 = pol2; pol2 = tmp;
  }
  bl_index = stand1*(stand1+1)/2+stand2;

  f1 = data[( (chan*NUM_BASELINES + bl_index)*NUM_POLS+pol1*2+pol2 )*NUM_NUMS_PER_COMPLEX];
  f2 = data[( (chan*NUM_BASELINES + bl_index)*NUM_POLS+pol1*2+pol2 )*NUM_NUMS_PER_COMPLEX+1];

  return complex<float>(f1*16, f2*16);
}


char *get_data_size(string filename) {
    struct stat stat_buf;
    static char file_size[1024];
    int rc = stat(filename.c_str(), &stat_buf);
    if ( rc != 0 ) {
      cerr << "Failed to stat file " << filename << endl;
      exit(1);
   }
   sprintf(file_size, "%ld", stat_buf.st_size-HEADER_SIZE);
   return file_size;
}

char *get_file_name(string filename) {
  static char full_name[PATH_MAX];
  static char host_name[PATH_MAX];
  gethostname(host_name, PATH_MAX);
  realpath(filename.c_str(), full_name);
  strcat(host_name,":"); strcat(host_name, full_name);
  return host_name;
}

static void hdf5_to_dada(const char *hdf5_file_name, const char *dada_file_name) {
  hid_t file_id, dataset_id, space_id, memspace_id, dataspace_id;
  int ndims;
  char header[HEADER_SIZE];
  hsize_t *dims;
  int num_floats, num_dumps_in_time, num_subbands;

  ofstream dada_file(dada_file_name, ios::out | ios::binary);
  if ( !dada_file ) {
    cerr << "Failed to write to " << dada_file_name << endl;
    exit(1);
  }
  if ( (file_id = H5Fopen(hdf5_file_name, H5F_ACC_RDONLY, H5P_DEFAULT)) < 0 ) {
    cerr << "Failed to open DADA file " << hdf5_file_name << endl;
    exit(1);
  }

  memset(header, 0, HEADER_SIZE);
  if ( H5LTread_dataset_char (file_id, "/HEADERS", header) < 0 ) {
    cerr << "Failed to read header from " << hdf5_file_name << endl;
    exit(1);
  }
  dada_file.write(header, HEADER_SIZE);

  dataset_id = H5Dopen(file_id,"/DADA",H5P_DEFAULT);

  // Find out the size by finding the dimensions
  space_id = H5Dget_space(dataset_id); 
  ndims = H5Sget_simple_extent_ndims(space_id); 
  if ( (dims = (hsize_t*)malloc(ndims*sizeof(hsize_t))) == NULL ) { 
     cerr << "Failed to malloc for ndims\n";
     exit(1);
  }
  H5Sget_simple_extent_dims(space_id,dims,NULL); 
  //for (i=0; i<ndims; ++i) printf("%lld\n",dims[i]);

  num_dumps_in_time = dims[0];
  num_subbands = dims[1];
  num_floats= dims[2];

  hsize_t     count[ndims];             
  hsize_t     offset[ndims];             
  hsize_t     stride[ndims];
  hsize_t     block[ndims];

  for (int i=0; i<ndims; ++i) {
    stride[i] = block[i] = 1;
    offset[i] = 0;
  }

  count[0]  = 1;
  count[1]  = 1;
  count[2] = num_floats;

  memspace_id = H5Screate_simple(ndims, count, NULL); 
  dataspace_id = H5Dget_space (dataset_id);

  int *buffer = new int[dims[2]];
  float *float_buffer = new float[dims[2]];

  for (int i=0; i<num_dumps_in_time; ++i)
    for (int j=0; j<num_subbands; ++j) {

    offset[0] = i;
    offset[1] = j;

    H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, stride, count, block);
    H5Dread (dataset_id, H5T_NATIVE_INT, memspace_id, dataspace_id, H5P_DEFAULT, buffer);

    for (int i=0; i<dims[2]; ++i) float_buffer[i] = ((float)buffer[i])*256;
    dada_file.write((const char*)float_buffer, dims[2]*sizeof(float));
  }

  dada_file.close();

}

const char *current_date_time() {
    time_t     now = time(0);
    struct tm  tstruct;
    static char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d %X", &tstruct);

    return buf;
}

static int *make_integer(float *buffer) {

  for (int i=0; i<NUM_CHANS*(NUM_BASELINES+NUM_SKY_STATES*NUM_OUTRIGGER_BASELINES)*NUM_POLS*NUM_NUMS_PER_COMPLEX; ++i) {
    int_mem_space[i] = (int)(buffer[i]/256);
  }
  return int_mem_space;
}

void check_consistent(const char *fname) {
  string line;
  vector <key_value> header0;
  int num_subbands, num_files_in_time;
  vector<string> words;

  // Read some values from the first line of the specification file
  ifstream file_list(fname);
  if ( !file_list.is_open() ) {
    cerr << "Can't open filelist " << fname << endl;
    exit(1);
  }

  num_subbands = 0;
  if ( !getline(file_list, line) ) {
    cerr << "Failed to read file list\n";
    exit(1);
  words = split(line);
  if ( words.size() != 3 ) {
    cerr <<  "Expecting 3 numbers at start of file list " << line << endl;
    exit(1);
  }
  num_files_in_time = atoi(words[0].c_str());
  num_subbands = atoi(words[1].c_str());

  for (int i=0; i<num_files_in_time; ++i)
    for (int j=0; j<num_subbands; ++j) {
      if ( !getline(file_list, line) ) {
        cerr << "Failed to read file list, tried to go past end of file\n";
        exit(1);
      }
  
    DadaReader dada_file(line.c_str());

    if ( i == 0 && j == 0 ) header0 = dada_file.get_header();

    // Check 
    for (int k=0; k<header0.size(); ++k) {
      if ( !dada_file.in_header(header0[k].key.c_str()) ) 
         cerr << "Warning, " << header0[k].key << " is missing in " << line << endl;
      //cout << header0[k].key << " " << dada_file.get_header_value(header0[k].key.c_str()) << "\n";
      if ( header0[k].value != dada_file.get_header_value(header0[k].key.c_str()) )
	cerr << "Warning, " << header0[k].key << " has different value in " << line << endl;
    }
  }
}}

int main(int argc, char *argv[]) {
  int num_files_in_time, num_subbands, num_scans_in_file;
  vector<string> words, files;
  vector <key_value> header;
  string line;


#ifdef DECOMPRESS
  if ( argc != 3 ) {
    cerr << "Usage: hdf52dada <input HDF5> <output DADA>\n";
    return 1;
  }
  hdf5_to_dada(argv[1], argv[2]);
  return 0;
#endif
  
  if ( argc != 3 ) {
    cerr << "Usage: dada2hdf5  <input file descriptions file> <output H5 file>\n";
    return 1;
  }

  check_consistent(argv[1]); 

  // Get information about the files

  // Read some values from the first line of the specification file
  ifstream file_list(argv[1]);
  if ( !file_list.is_open() ) {
    cerr << "Can't open filelist " << argv[1] << endl;
    return 1;
  }

  num_subbands = 0;
  if ( !getline(file_list, line) ) {
    cerr << "Failed to read file list\n";
    return 1;
  }
  words = split(line);
  if ( words.size() != 3 ) {
    cerr <<  "Expecting 3 numbers at start of file list " << line << endl;
    exit(1);
  }
  num_files_in_time = atoi(words[0].c_str());
  num_subbands = atoi(words[1].c_str());
  num_scans_in_file = atoi(words[2].c_str());

  cout << "HDF5 file will contain " << num_scans_in_file*num_files_in_time << "x" << num_subbands << " (time x subband) correlator dumps\n";

  // Setup HDF5 variables. Need to check the response code for all these calls.
  hsize_t dims[] = { num_files_in_time*num_scans_in_file, num_subbands, NUM_CHANS*(NUM_BASELINES+NUM_SKY_STATES*NUM_OUTRIGGER_BASELINES)*NUM_POLS*NUM_NUMS_PER_COMPLEX };
  int rank = sizeof(dims)/sizeof(hsize_t);
  string subbands[num_subbands];
  hsize_t count[rank];             
  hsize_t offset[rank];             
  hsize_t stride[rank];
  hsize_t block[rank];
  hid_t	file_id, dataset_id, memspace_id, dataspace_id, dcpl_id, group_id;

  // This defines the chunk size - it is one correlator dump, so the extent is ony 1 in the time and 
  // frequency directions (first 2)
  count[0]  = 1;
  count[1]  = 1;
  count[2] = NUM_CHANS*(NUM_BASELINES+NUM_SKY_STATES*NUM_OUTRIGGER_BASELINES)*NUM_POLS*NUM_NUMS_PER_COMPLEX;

  for (int i=0; i<rank; ++i) {  // defaults
    stride[i] = block[i] = 1;
    offset[i] = 0;
  }


  // Start HDF5 operations -----------------------------------------------------------------------

  // Create a new file using default properties.
  if ( (file_id=H5Fcreate(argv[2], H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT)) < 0 ) {
    cerr << "Failed to create HDF5 file " << argv[2] << endl;
    return 1;
  }

  // Create group for meta data
  if ( (group_id=H5Gcreate2(file_id, "/INFORMATION", H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT)) < 0 )  {
    cerr << "Failed to create group for INFORMATION\n";
    return 1;
  }

  if ( H5LTset_attribute_string(file_id, "/INFORMATION", "PROGRAM_VERSION", PROGRAM_VERSION) < 0 )  {
    cerr << "Failed to set attribute " <<  "PROGRAM_VERSION" << endl;
    return 1;
  }

  if ( H5LTset_attribute_string(file_id, "/INFORMATION", "CREATION_DATETIME", current_date_time()) < 0 )  {
    cerr << "Failed to set attribute " <<  "PROGRAM_VERSION" << endl;
    return 1;
  }

  int comp = COMPRESSION_ID;
  if ( H5LTset_attribute_int(file_id, "/INFORMATION", "COMPRESSION_ID", &comp, 1) < 0 )  {
    cerr << "Failed to set attribute " <<  "COMPRESSION_ID" << endl;
    return 1;
  }

  // Create the data space for the dataset. 
  if ( (dataspace_id=H5Screate_simple(rank, dims, NULL)) < 0 ) {
    cerr << "Failed to create dataspace\n";
    return 1;
  }


  // Create a dataset creation property list and set it to use filter and chunking

  if ( (dcpl_id=H5Pcreate(H5P_DATASET_CREATE)) < 0 ) {
    cerr << "Failed to create HDF5 property list\n";
    return 1;
  }

  if ( H5Pset_chunk(dcpl_id, rank, count) < 0 ) {
    cerr << "Failed to set HDF5 chunk size\n";
    return 1;
  }

#ifdef COMPRESS
	//https://github.com/kiyo-masui/bitshuffle/blob/c5c928fe7d4bc5b9391748a8dd29de5a89c3c94a/lzf/example.c
  if( H5Pset_filter (dcpl_id, 32008, H5Z_FLAG_OPTIONAL, 0, NULL) < 0 ) {
    fprintf(stderr, "Failed to install bitshuffle filter\n");
    return 1;
  }

  if ( H5Pset_filter (dcpl_id, 32004, H5Z_FLAG_OPTIONAL, 0, NULL) < 0 )  {
    fprintf(stderr, "Failed to install LZ4 filter\n");
    return 1;
  }
#endif

  // Create the dataset, but this won't allocate any space.
  if ( (dataset_id=H5Dcreate2(file_id, "/DADA", H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, dcpl_id, H5P_DEFAULT)) < 0 )  {
    cerr << "Failed to create the dataset in the HDF5 file\n";
    return 1;
  }

 
  // Create a buffer memory for creating hyperslabs, which are slices in the dataset
  // corresponding to correlator dumps
  if ( (memspace_id=H5Screate_simple(rank, count, NULL)) < 0 ) {
    cerr << "Failed to create the memspace for hyperslab\n";
    return 1;
  }
  if ( (dataspace_id=H5Dget_space(dataset_id)) < 0 ) {
    cerr << "Failed to extract the space id from the dataset\n";
    return 1;
  }
 
  char headers[num_files_in_time*num_subbands*HEADER_SIZE];	// save them all
  memset(headers, 0, HEADER_SIZE);

  // Loop through the DADA data, getting correlator dumps and inserting them into the HDF5
  // in the right places.
  for (int i=0; i<num_files_in_time; ++i)
    for (int j=0; j<num_subbands; ++j) {

      if ( !getline(file_list, line) ) {
        cerr << "Failed to read file list, tried to go past end of file\n";
        return 1;
      }
      files.push_back(line);

      cout << "Opening file " << line << endl;
      DadaReader dada_file(line.c_str());
      
      if ( dada_file.in_header("CFREQ") ) subbands[j] = dada_file.get_header_value("CFREQ");
      else {
        cerr << "Missing CFREQ in file " << line << endl;
        exit(1);
      }

      for (int k=0; k<num_scans_in_file; ++k) {
	cout << "Writing scan " << i*num_scans_in_file+k << " subband " << j << endl;

        offset[0] = i*num_scans_in_file+k; offset[1] = j;

        if ( H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, stride, count, block) < 0 ) {
          cerr << "Failed to select hyperslab in the HDF5 file\n";
          return 1;
        }

        // This is where the data actually goes in the HDF file
        dada_file.next_time_scan();
        //cout << get_vis_value(dada_file.raw_data(), 45, 12, 12, 1, 1)  << endl;  
        if ( H5Dwrite(dataset_id, H5T_NATIVE_INT, memspace_id, dataspace_id, H5P_DEFAULT, make_integer(dada_file.raw_data())) < 0 ) {
          cerr << "Failed to write data to HDF5 file\n";
          return 1;
        }
      }

      strcpy(headers+i*num_subbands+j*HEADER_SIZE, dada_file.get_header_string());
      strcat(headers+i*num_subbands+j*HEADER_SIZE, "FILE_NAME ");
      strcat(headers+i*num_subbands+j*HEADER_SIZE, get_file_name(line.c_str()));
      strcat(headers+i*num_subbands+j*HEADER_SIZE, "\nDATA_SIZE ");
      strcat(headers+i*num_subbands+j*HEADER_SIZE, get_data_size(line));
      strcat(headers+i*num_subbands+j*HEADER_SIZE, "\n");
    }

  hsize_t header_dims[3] = { num_files_in_time, num_subbands, HEADER_SIZE };
  H5LTmake_dataset_char (file_id, "/HEADERS", 3, header_dims, headers);

  int n_scans = num_scans_in_file*num_files_in_time;
  if ( H5LTset_attribute_int(file_id, "/DADA", "NUM_SCANS", &n_scans, 1) < 0 )  {
    cerr << "Failed to set attribute NUM_FILES\n";
    return 1;
  }

  if ( H5LTset_attribute_int(file_id, "/DADA", "NUM_SUBBANDS", &num_subbands, 1) < 0 )  {
    cerr << "Failed to set attribute NUM_SUBBANDS\n";
    return 1;
  }



  // Everything gets closed here

  return 0;
     

}

