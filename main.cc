/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


// For testing

#include <cstdlib>
#include <iostream>
#include "header.h"
#include "cables.h"
#include "stands.h"
#include "uvdata.h"
#include "globals.h"
#include "dada_reader.h"

int main(int argc, char *argv[]) {

  if ( argc != 5 ) {
    cerr << "Usage: duct <header file> <antenna file> <instrument config file> <dada file>\n";
    exit(1);
  }



  Header header(argv[1]); 
  UVWdata uvwdata(header, noLockPointing);
  Stands stands(argv[2]);
  Cables cables(argv[3]); 


  uvwdata.make_uvw(cables, stands, noTransform, doLockPointing);
  
  return 0;
}




