/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


/*
 Provides the C API. There is one function call, which loads everything.
*/
#include <stdio.h>
#include <cstdlib>
#include <iostream>
#include "header.h"
#include "cables.h"
#include "stands.h"
#include "uvdata.h"
#include "globals.h"
#include "dada_reader.h"
#include "duct.h"


#ifdef NOT
typedef struct _uvdata {
    int  n_pol;
    int  pol_type;        /* index to signal what kind of data: 1: Stokes, -1: Circ, -5: Linear */
    int  n_IF;            /* number of IFs in the data */
    int  n_freq;          /* number of freq channels if there is only 1 IF */
    int  n_vis;           /* number of sets of visibilities, one for each time instant */
    float cent_freq;      /* Hz */
    float freq_delta;
    double *date;         /* Julian date. array the size of n_vis */
    int  *n_baselines;    /* array the size of n_vis. number of baselines for each scan. */
    source_table *source; /* a pointer to a source table */
    array_data *array;    /* a pointer to an array struct */
    float **visdata;      /* array the size of n_vis whose elements point to arrays of visibiliites
                             the size of n_pol*n_freq*n_baselines complex floats. The data are ordered so
                             that pol changes most quickly, then freq, and baseline most slowly. Index with:
                             visdata[baseline*(n_pol*n_freq) + freq*n_pol + pol] as a float complex */
    float **weightdata;   /* weight data for visibiliites. Same data ordering as above, just float,
                             not complex */
    float **baseline;     /* same ordering again. encoded baseline using Miriad encoding convention */
    double **u;           /* arry the size of n_vis whose elements point to arrays of uvw
                             data the size of n_baselines */
    double **v;
    double **w;
    fq_table *fq;         /* pointer to array of FQ table for multi-IF data */
} uvdata;

typedef struct _array_table {
    int   n_ant;
    double xyz_pos[3];    /* the X,Y,Z coord of the array in conventional radio astronomy units */
    char  name[16];       /* name of the telescope (turns into TELESCOP keyword in fits file */
    char  instrument[16]; /* name of the instrument (turns into the INSTRUME keyword in fits file */
    ant_table *antennas;  /* a pointer to an array of ant_tables the size of the number of antennas */
    double arr_lon_rad;   /* array centre lon in radian */
    double arr_lat_rad;   /* array centre lat in radian */
} array_data;

typedef struct  _ant_table {
    char name[SIZE_ANT_NAME+1];
    double xyz_pos[3];			/* position relative to array centre  */
    float  xyz_deriv[3];
    /* skip orbital params for now */
    int  stand_num;
    int  mount_type;
    double axis_offset[3];
    float pol_angleA;
    float pol_calA;
    float pol_angleB;
    float pol_calB;
    char pol_typeA[SIZE_POL_TYPE];
    char pol_typeB[SIZE_POL_TYPE];
} ant_table;

UV 4 0 1 109 10 47004000.000000 24000.000000
	2457057.261881 2457057.261985 2457057.262089 2457057.262193 2457057.262297 2457057.262402 2457057.262506 2457057.262610 2457057.262714 2457057.262818 
UV 0x6da19c70 0xa450b00 0xa450d50 0xa450b60 0xa450bc0 0xa450c20 0xa450c80 (nil)
ARR 256 -2409247.382086 -4477889.836796 3839328.456740 :: :: 0xa450dd0 0.000000 0.000000
SO :Zenith: 0 0 :: 0 289.456638 37.211583
ANT :Stand1: -44.683926 68.373000 58.924565 0.000000 0.000000 0.000000 1 0 0.000000 0.000000 0.000000 0.000000 0.000000 90.000000 0.000000 :X: :Y:

#endif

// Put this in cuwarp or corr2uvfits to test what it loads
void print(uvdata *data) {
  int i;
  int which_scan;

  if ( data->n_vis == 1 ) which_scan = 0;
  else which_scan = data->n_vis/2;


  printf("n_pol: %d pol_type: %d n_IF: %d n_freq: %d n_vis: %d cent_freq: %f freq_delta: %f\n",
    data->n_pol,data->pol_type,data->n_IF,data->n_freq,data->n_vis,data->cent_freq,data->freq_delta);

  printf("Dates:\n");
  for (i=0; i<data->n_vis; ++i) printf("%f ",data->date[i]); printf("\n");

  if ( data->n_baselines == NULL ) printf("n_baselines is NULL\n");
  else {
    printf("Baselines per scan:\n");
    for (i=0; i<data->n_vis; ++i) printf("%d ",data->n_baselines[i]); printf("\n");
  }

  if ( data->visdata == NULL ) printf("visdata is NULL\n");
  else {
    printf("visdata scan %d:\n",which_scan);
    for (i=0; i<5; ++i) printf("%d: %f %f\n",i,data->visdata[which_scan][i*2],data->visdata[which_scan][i*2+1]); 
    for (i=data->n_baselines[which_scan]-5; i<data->n_baselines[which_scan]; ++i) printf("%d: %f %f\n",i,data->visdata[which_scan][i*2],data->visdata[which_scan][i*2+1]); 
  }
  if ( data->weightdata == NULL ) printf("weightdata is NULL\n");
  else {
    printf("weightdata scan %d:\n",which_scan);
    for (i=0; i<5; ++i) printf("%d: %f\n",i,data->weightdata[which_scan][i]); 
  }
  if ( data->baseline == NULL ) printf("baseline is NULL\n");
  else {
    printf("baseline from scan %d:\n",which_scan);
    for (i=0; i<5; ++i) printf("%d: %f\n",i,data->baseline[which_scan][i]); 
    for (i=data->n_baselines[which_scan]-5; i<data->n_baselines[which_scan]; ++i) printf("%d: %f\n",i,data->baseline[which_scan][i]); 
  }

  if ( data->u == NULL ) printf("u is NULL\n");
  if ( data->v == NULL ) printf("v is NULL\n");
  if ( data->w == NULL ) printf("w is NULL\n");
  if( data->u != NULL && data->v != NULL && data->w != NULL ) {
    printf("uvw from scan %d:\n",which_scan);
    for (i=0; i<5; ++i) printf("%d: %f %f %f\n",i,data->u[which_scan][i]*VLIGHT,data->v[which_scan][i]*VLIGHT,data->w[which_scan][i]*VLIGHT); 
    for (i=data->n_baselines[which_scan]-5; i<data->n_baselines[which_scan]; ++i) printf("%d: %f %f %f\n",i,data->u[which_scan][i]*VLIGHT,data->v[which_scan][i]*VLIGHT,data->w[which_scan][i]*VLIGHT); 
  }

  printf("Source:\n");
  printf("name: %s id: %d qual: %d calcode: %s freq_id: %d ra: %f dec: %f\n",
    data->source->name,data->source->id,data->source->qual,data->source->calcode, data->source->freq_id,data->source->ra,data->source->dec);

  printf("Array:\n");
  printf("n_ant: %d xyz_pos[0]: %f xyz_pos[1]:%f xyz_pos[2]: %f name: %s instrument: %s arr_lon_rad: %f arr_lat_rad: %f\n",
    data->array->n_ant,data->array->xyz_pos[0],data->array->xyz_pos[1],data->array->xyz_pos[2],
    data->array->name,data->array->instrument,data->array->arr_lon_rad,data->array->arr_lat_rad);

  printf("Array antennas:\n");
  for (i=0; i<5; ++i) {
    printf("%d: name: %s xyz_pos[0]: %f xyz_pos[1]: %f xyz_pos[2]: %f xyz_deriv[0]: %f xyz_deriv[1]: %f xyz_deriv[2]: %f stand_num: %d mount_type: %d\n",
	i,data->array->antennas[i].name,
        data->array->antennas[i].xyz_pos[0],data->array->antennas[i].xyz_pos[1], data->array->antennas[i].xyz_pos[2],
        data->array->antennas[i].xyz_deriv[0],data->array->antennas[i].xyz_deriv[1], data->array->antennas[i].xyz_deriv[2],
        data->array->antennas[i].stand_num, data->array->antennas[i].mount_type);
    printf("  axis_offset[0]: %f axis_offset[1]: %f axis_offset[2]: %f pol_angleA: %f pol_calA: %f pol_angleB: %f pol_calB: %f pol_typeA: %s pol_typeB: %s\n",
        data->array->antennas[i].axis_offset[0], data->array->antennas[i].axis_offset[1], data->array->antennas[i].axis_offset[2],
        data->array->antennas[i].pol_angleA, data->array->antennas[i].pol_calA, data->array->antennas[i].pol_angleB,
        data->array->antennas[i].pol_calB,
        data->array->antennas[i].pol_typeA, data->array->antennas[i].pol_typeB);
  }
  for (i=data->array->n_ant-5; i<data->array->n_ant; ++i) {
    printf("%d: name: %s xyz_pos[0]: %f xyz_pos[1]: %f xyz_pos[2]: %f xyz_deriv[0]: %f xyz_deriv[1]: %f xyz_deriv[2]: %f stand_num: %d mount_type: %d\n",
	i,data->array->antennas[i].name,
        data->array->antennas[i].xyz_pos[0],data->array->antennas[i].xyz_pos[1], data->array->antennas[i].xyz_pos[2],
        data->array->antennas[i].xyz_deriv[0],data->array->antennas[i].xyz_deriv[1], data->array->antennas[i].xyz_deriv[2],
        data->array->antennas[i].stand_num, data->array->antennas[i].mount_type);
    printf("  axis_offset[0]: %f axis_offset[1]: %f axis_offset[2]: %f pol_angleA: %f pol_calA: %f pol_angleB: %f pol_calB: %f pol_typeA: %s pol_typeB: %s\n",
        data->array->antennas[i].axis_offset[0], data->array->antennas[i].axis_offset[1], data->array->antennas[i].axis_offset[2],
        data->array->antennas[i].pol_angleA, data->array->antennas[i].pol_calA, data->array->antennas[i].pol_angleB,
        data->array->antennas[i].pol_calB,
        data->array->antennas[i].pol_typeA, data->array->antennas[i].pol_typeB);
  }
  if ( data->fq == NULL )  printf("fq is NULL\n");
  else {
    printf("fq:\n");
    printf("freq: %f chbw: %f bandwidth: %f sideband: %d\n",data->fq->freq,data->fq->chbw,data->fq->bandwidth,data->fq->sideband);
  }
}


void import_telescope_observation(uvdata **newdata, const char *header_file, const char *stands_file, const char* cables_file, const char *dada_file, 
				const int which_scan, const float vis_scale) {
  uvdata *data;

  Header header(header_file); 
  UVWdata *uvwdata = new UVWdata(header, doLockPointing, which_scan);  
  Stands stands_for_ant(stands_file);
  Stands stands_for_uvw(stands_for_ant);	// a copy of stands_for_ant
  Cables cables(cables_file); 

  (*newdata) = data = (uvdata*)malloc(sizeof(uvdata)); bzero(data, sizeof(uvdata));
  

  data->n_pol = NUM_POLS;
  data->pol_type = POL_TYPE;
  data->n_IF = 1;
  data->fq = NULL;
  data->n_freq = uvwdata->n_chans;
  data->n_vis = uvwdata->n_scans;
  data->cent_freq = uvwdata->cent_freq_Hz;
  data->freq_delta = uvwdata->channel_width_Hz;
  data->date = (double*)malloc(uvwdata->n_scans*sizeof(double));
  for (int i=0; i<uvwdata->n_scans; ++i) data->date[i] = uvwdata->dates[i]; 
  data->n_baselines = (int*)malloc(uvwdata->n_scans*sizeof(int));
  for (int i=0; i<uvwdata->n_scans; ++i) data->n_baselines[i] = NUM_BASELINES;

  data->source = (source_table*)malloc(sizeof(source_table));
  for (int i=0; i<header.field_name.size(); ++i) data->source->name[i] = header.field_name[i]; data->source->name[header.field_name.size()] = '\0';
  data->source->id = data->source->qual = 0;
  data->source->calcode[0] = '\0';
  data->source->freq_id = 0;
  data->source->ra = uvwdata->raDeg();
  data->source->dec = uvwdata->decDeg();

  data->array = (array_data*)malloc(sizeof(array_data));
  data->array->n_ant = NUM_STANDS;
  data->array->xyz_pos[0] = stands_for_ant.X; data->array->xyz_pos[1] = stands_for_ant.Y; data->array->xyz_pos[2] = stands_for_ant.Z;
  strcpy(data->array->name, "LWA-OVRO"); strcpy(data->array->instrument, data->array->name); 
  data->array->antennas = (ant_table*)malloc(NUM_STANDS*sizeof(ant_table));
  stands_for_ant.transform(0, uvwdata->raHrs(), uvwdata->decDeg(), doXYZ);
  for (int i=0; i<NUM_STANDS; ++i) {
    for (int j=0; j<stands_for_ant[i].name.size(); ++j) data->array->antennas[i].name[j] = stands_for_ant[i].name[j]; 
    data->array->antennas[i].name[stands_for_ant[i].name.size()] = '\0';

    data->array->antennas[i].xyz_pos[0] = stands_for_ant[i].east_x;
    data->array->antennas[i].xyz_pos[1] = stands_for_ant[i].north_y;
    data->array->antennas[i].xyz_pos[2] = stands_for_ant[i].height_z;

    data->array->antennas[i].xyz_deriv[0] = 0;
    data->array->antennas[i].xyz_deriv[1] = 0;
    data->array->antennas[i].xyz_deriv[2] = 0;

    data->array->antennas[i].stand_num = stands_for_ant[i].stand_num+1;    // note 1-based index now

    data->array->antennas[i].mount_type = 0;
    data->array->antennas[i].axis_offset[0] = data->array->antennas[i].axis_offset[1] = data->array->antennas[i].axis_offset[2] = 0.0;
    data->array->antennas[i].pol_angleA = 0.0; data->array->antennas[i].pol_calA = 0.0; 
    data->array->antennas[i].pol_angleB = 90.0; data->array->antennas[i].pol_calA = 0.0; 
    data->array->antennas[i].pol_typeA[0] = 'X'; data->array->antennas[i].pol_typeA[1] = '\0';
    data->array->antennas[i].pol_typeB[0] = 'Y'; data->array->antennas[i].pol_typeB[1] = '\0';
  }
  data->array->arr_lon_rad = ARR_LON_RAD; 
  data->array->arr_lat_rad = ARR_LAT_RAD;

  if ( which_scan == NO_SCANS ) return;

  uvwdata->make_uvw(cables, stands_for_uvw, doPrecess, doLockPointing);

  data->u = uvwdata->u;
  data->v = uvwdata->v;
  data->w = uvwdata->w;


  uvwdata->load_visibilities(dada_file, cables, header, doCableDelay, vis_scale);

  data->visdata = (float**)uvwdata->visdata;     // was complex i.e. 2 floats

  data->weightdata = uvwdata->weightdata;

  data->baseline = (float**)malloc(data->n_vis*sizeof(float*));
  data->baseline[0] = uvwdata->baselines.codes;
  for (int i=1; i<data->n_vis; ++i) {
    data->baseline[i] = (float*)malloc(NUM_BASELINES*sizeof(float));
    memcpy(data->baseline[i], data->baseline[0], NUM_BASELINES*sizeof(float));    
  }
 
}
