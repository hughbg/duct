# README #


This contains the DuCT classes and program (DADA - CUWARP Translator). The classes extract data from a DADA file for passing to cuwarp in an internal structure; the program can print information for testing. It contains algorithms from corr2uvfits written by Randall Wayth and others. DuCT is necessary because the DADA format has changed, and because different flagging mechanisms will be used. It provides a library that incorporates directly into cuwarp so cuwarp opens and reads the DADA file itself, through the DuCT library.

The Makefile indicates how to build the program. You need to have the PAL library installed as PAL functions are used (SLALIB calls in corr2uvfits have been replaced with PAL). Some necessary Starlink/BSD files are in the star/ directory. 

To use DuCT, link the library libduct.a into cuwarp. There is a single function to call - "import_telescope_observation" - which is declared in duct.h. Modify cuwarp so that readUVFits is replaced with the call to import_telescope observation.
