/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <fstream>
#include <cmath>
#include <iostream>
#include <sstream>
#include <cstdlib>
#include "flags.h"
#include "globals.h"


// Class Flags  ------------------------------------------------------


/******************************
 Read the flags from a text
 file and populate the list.
*******************************/
Flags::Flags(const char* filename) {

  ifstream file(filename);
  string line;
  int num=0;
  int stand1, stand2, time, channel;
  vector <string> words;

  flags = new scan[10];

  for (int t=0; t<10; ++t) for (int i=0; i<NUM_STANDS; ++i) for (int j=0; j<NUM_STANDS; ++j) for (int ch=0; ch<NUM_CHANS; ++ch) flags[t][i][j][ch] = false; 

  if ( file.is_open() ) {
    while( getline(file, line) ) 
      if ( line.size() > 0 && line[0] != '#' ) {
	words = split(line); 
        if ( words.size() != 4 ) {
          cerr <<  "Flags file invalid at line " << line << endl;
          exit(1);
        }

        time = atoi(words[0].c_str()); stand1 = atoi(words[1].c_str()); stand2 = atoi(words[2].c_str()); channel = atoi(words[3].c_str());

	if ( stand1 < 0 || NUM_STANDS <= stand1 || stand2 < 0 || NUM_STANDS <= stand2 ) {
          cerr <<  "Stand number invalid in flags file at " << words[1] << " " << words[2] << endl;
          exit(1);
        }

	flags[time][stand1][stand2][channel] = true;
        ++num;

      }
    cout << "Loaded " << num << " flags\n";
  } 
}

/*

int main() {
  Flags list("flags.txt");
 
  list.print();
  

  return 0;
}*/
