/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DUCT_H
#define DUCT_H
#include "uvfits.h"

#define ALL_SCANS -5432
#define NO_SCANS -4321
#define HEADER_ONLY NO_SCANS

#ifdef __cplusplus
extern "C" {
#endif


// This is the function that does all the work. Call this, and it will fill the internal data structure newdata.
void import_telescope_observation(uvdata **newdata, const char *header_file, const char *stands_file, const char* cables_file, const char *dada_file, 
		const int one_scan_only, const float vis_scale);

// Use this to dump the data structure for debugging
void print(uvdata *data);

#ifdef __cplusplus
}
#endif

#endif
