/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <sstream>
#include "globals.h"
using namespace std;

vector<string> split(string line) {
   vector<string> tokens;
   string token;

   for (int i=0; i<line.size(); ++i) if ( line[i] == '\t' ) line[i] = ' ';
   stringstream l(line);

   while ( getline(l, token, ' ') )
     if ( token.size() > 0 ) tokens.push_back(token);

   return tokens;
}

void mat_transpose(double rmat1[3][3], double rmat2[3][3]) {
  int i, j;

  for(i=0;i<3;++i) {
    for(j=0;j<3;++j) {
      rmat2[j][i] = rmat1[i][j];
    }
  }

}

int get_num_at_end(string s) {
  int i=0, num=0;
  while ( isalpha(s[i]) ) ++i; 
  while ( i < s.size() ) { num = num*10+(s[i]-'0'); ++i; }
  return num-1; 
}

