/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DADA_READER_H
#define DADA_READER_H

/*
 Sequentially read the data in a DADA file. Does not read the header. Data is
 read in blocks of time scans. Once loaded, a scan can be array-indexed to 
 extract any visibility value using multi-dimensional indexing.
*/ 


#include <fstream>
#include <complex>
#include "globals.h"
#include "duct.h"

using namespace std;

static const int DADA_HEADER_KEYS_NUM = 40;

struct key_value {
  string key, value;
};

class DadaReader {
private:
  ifstream dada;          // DADA file handle
  int time_scan_loaded;
  complex<float> *data;   // internal cache
  float vis_scale;
  struct key_value header[DADA_HEADER_KEYS_NUM];

  // Some functions for header key/value manipulation
  void parse_header(char *header_str);  

public:


  int outrigger_start_index;
  float max;

  DadaReader(const char *filename, const int which_scan = ALL_SCANS, const float scale_factor = 1);
  ~DadaReader();

  // load a new, or the first, time scan data into cache
  void next_time_scan();

  // array index the data

  // Get a vsiibility value in the data using indexes of channel, baseline, polarization
  complex<float> get_vis_value(int chan, int stand1, int stand2, int pol1, int pol2);

  // Get a visibility value in the outrigger data using indexes of channel, baseline, polarization
  // This is probably wrong now.
  complex<float> get_outrigger_value(int state, int chan, int index, int pol1, int pol2) {
    return data[FULL_SIZE+state*NUM_CHANS*NUM_OUTRIGGER_BASELINES*NUM_POLS + chan*NUM_OUTRIGGER_BASELINES*NUM_POLS + index*NUM_POLS + pol1*2 + pol2]; }

  // From two stand numbers, find out the index in the DADA file
  int dada_index(int dada1_index, int dada2_index) {     // dada1_index > dada2_index for triang DADA.
    return dada1_index*(dada1_index+1)/2+dada2_index; }  

  // From two stand numbers that are on an outrigger baseline, find out the index in the DADA file
  int outrigger_index(int dada1_index, int dada2_index) { return dada_index(dada1_index, dada2_index)-outrigger_start_index;  }

  float *raw_data() { return (float*)data; } 

  void swap(int* i1, int* i2) { int tmp=*i1; *i1=*i2; *i2=tmp; }

  // Header functions
  bool in_header(const char *key);
  string get_header_value(const char *key);


};

#endif
