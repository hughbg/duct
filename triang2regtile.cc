/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/

/*
  Converts TIME_SUBSET_CHAN_TRIANGULAR_POL_POL_COMPLEX to REG_TILE_TRIANGULAR_2x2 DADA file format. 
  Some values in the header are adjusted, and the data chunks are reformatted.
*/

#include <cstring>
#include <iostream>
#include <string>
#include "dada_reader.h"

using namespace std;

// Header keywords i nthe oredr in which they usually appear. I want to preserve the order.
static const char *REG_HEADER_ORDER[] = { "BW","RA","DEC","FREQ","HDR_SIZE","HDR_VERSION","INSTRUMENT","MODE","NBIT","NCHAN", "NDIM","NPOL","NSTATION", 
		"XENGINE_NTIME","NAVG","DATA_ORDER","SKY_STATE_PHASE","BYTES_PER_AVG","BYTES_PER_SECOND","OBS_OFFSET", 
		"PID","RECEIVER","SOURCE","TELESCOPE","TSAMP","PROC_FILE","OBS_XFER","CFREQ","UTC_START","FILE_NUMBER","FILE_SIZE" };

// Utilities
template <typename T> string to_string(const T& n) {
  std::ostringstream stm;
  stm << n;
  return stm.str();
}

bool is_integer(char *s) {
  while ( '0' <= *s && *s <= '9' ) ++s;
  return *s == '\0';
}

/*
The new header for REG TILE has to have the OBS_OFFSET adjusted so that when the file goes through other programs,
the time will come out right. Other things to change are data sizes.
*/
void dump_new_header(DadaReader& dada_file, int nscan, int which_scan, const char *out_name) {
  string new_str;
  size_t new_obs_offset;
  const char *key;
  int str_len;
  ofstream out_file;

  cout << "Converting header\n";

  // Build a long string that is the DADA header
  for (int i=0; i<sizeof(REG_HEADER_ORDER)/sizeof(const char*); ++i) {
    key = REG_HEADER_ORDER[i]; 
    if ( strcmp(key, "DATA_ORDER") == 0 )
      new_str += "DATA_ORDER REG_TILE_TRIANGULAR_2x2\n";
    else if ( strcmp(key, "BYTES_PER_SECOND") == 0 ) {
      new_str += "BYTES_PER_SECOND ";
      new_str += to_string(REG_TILE_FRAME_SIZE_BYTES);
      new_str += "\n";
    } else if  ( strcmp(key, "BYTES_PER_AVG") == 0 ) {
      new_str += "BYTES_PER_AVG ";
      new_str += to_string(REG_TILE_FRAME_SIZE_BYTES);
      new_str += "\n";
    } else if  ( strcmp(key, "FILE_SIZE") == 0 ) {
      new_str += "FILE_SIZE ";
      new_str += to_string(nscan*REG_TILE_FRAME_SIZE_BYTES);
      new_str += "\n";
    } else if ( strcmp(key, "OBS_OFFSET") == 0 ) {
      new_obs_offset = atoll(dada_file.get_header_value(key).c_str());
      if ( which_scan != ALL_SCANS )
        new_obs_offset += TRIANGULAR_FRAME_SIZE_BYTES*which_scan;
      new_obs_offset = ((float)REG_TILE_FRAME_SIZE_BYTES/(float)TRIANGULAR_FRAME_SIZE_BYTES)*new_obs_offset;
      new_str += "OBS_OFFSET ";
      new_str += to_string(new_obs_offset);
      new_str += "\n";
    } else if ( dada_file.in_header(key) ) { 
      new_str += key;
      new_str += " ";  
      new_str += dada_file.get_header_value(key);
      new_str += "\n";
    }
  }

  str_len = new_str.length();
  while ( str_len < HEADER_SIZE ) { 
    new_str += "\0";
    ++str_len;
  }

  out_file.open(out_name, ios::out|ios::binary);
  if ( !out_file.is_open() ) {
    cerr << "Failed opening file " << out_name << endl;
    exit(1);
  }
  out_file.write((const char*)new_str.c_str(), HEADER_SIZE);
  out_file.close();
}

/*
Copied from Ben's leda_dbtransient code. Maps baselines from one form to another.

*/
void gen_baseline_mapping(int nstations, int *mapping, int& in_nbaseline) {
  int tile_nrow, tile_ncol, out_nbaseline, nstation_tiles, ntile,
	row_tile, col_tile, b_tile, in_b, out_b, tile_row, tile_col;

  tile_nrow = tile_ncol = 2;
  out_nbaseline  = nstations*(nstations+1)/2;
  nstation_tiles = (nstations-1)/tile_ncol+1;
  ntile = nstation_tiles*(nstation_tiles+1)/2;
  in_nbaseline = tile_nrow*tile_ncol*ntile;
  for (int i=0; i<nstations; ++i) {
    for (int j=0; j<=i; ++j) {
      out_b = i*(i+1)/2 + j;
      row_tile = i / tile_nrow;
      col_tile = j / tile_ncol;
      b_tile   = col_tile + row_tile*(row_tile+1)/2;
      tile_row = i % tile_nrow;
      tile_col = j % tile_ncol;
      in_b     = b_tile + ntile*(tile_col + tile_ncol*tile_row);
      mapping[out_b] = in_b;
      //cout << i << " " << j << " " << out_b << " " << in_b << endl;
    }
  }
       
}         



int main(int argc, char *argv[]) {
  int tri2rtt[NUM_BASELINES], in_nbaseline, in_b, n_scans, which_scan;
  float *reg_tile_data, *triang_data;
  ofstream out_file;
  
  // Input checks
  if ( argc != 4 ) {
    cerr << "Usage: triang2regtile <what> <in file> <outfile>. Convert a triangular DADA file to register tile format.\n\n";
    cerr << "what: If 'all' then convert all correlator dumps, otherwise specify a single dump to convert, with the index number 0 .. n_dumps-1.\n";
    cerr << "infile: A triangular format DADA file.\n";
    cerr << "outfile: The file to create.\n";
    exit(1);
  }

  if ( strcmp(argv[1],"all") == 0 ) which_scan = ALL_SCANS;
  else {
    if ( !is_integer(argv[1]) ) {
      cerr << "Expecting positive integer scan number\n";
      return 1;
    }
    else which_scan = atoi(argv[1]);
  }

  // Initialise the file cache
  DadaReader dada_file(argv[2], which_scan, 1);

  if ( which_scan != ALL_SCANS ) n_scans = 1;

  // Create a new REG_TILE header andwrite to file
  dump_new_header(dada_file, n_scans, which_scan, argv[3]);

  // Needed for the loop below
  gen_baseline_mapping(NUM_STANDS, tri2rtt, in_nbaseline);
  reg_tile_data = new float[REG_TILE_FRAME_SIZE_BYTES/sizeof(float)];

  // Loop over all dumps, converting data from one form to the other.
  for (int time=0; time<n_scans; ++time) {
    if ( which_scan != ALL_SCANS ) cout << "Converting dump " << which_scan << endl;
    else cout << "Converting dump " << time << endl;

    dada_file.next_time_scan();
    triang_data = dada_file.raw_data();

    for (int c=0; c<NUM_CHANS; ++c)
      for (int out_b=0; out_b<NUM_BASELINES; ++out_b) {     

        in_b = tri2rtt[out_b];
      
  	// This is copied from Ben's leda_dbtransients code, then reversed. So it goes from TRIANG to REG_TILE
        reg_tile_data[0+4*(in_b+in_nbaseline*(c+NUM_CHANS*(0)))] = triang_data[0+8*(out_b+NUM_BASELINES*(c))];
        reg_tile_data[0+4*(in_b+in_nbaseline*(c+NUM_CHANS*(1)))] = triang_data[1+8*(out_b+NUM_BASELINES*(c))];
        reg_tile_data[1+4*(in_b+in_nbaseline*(c+NUM_CHANS*(0)))] = triang_data[2+8*(out_b+NUM_BASELINES*(c))];
        reg_tile_data[1+4*(in_b+in_nbaseline*(c+NUM_CHANS*(1)))] = triang_data[3+8*(out_b+NUM_BASELINES*(c))];
        reg_tile_data[2+4*(in_b+in_nbaseline*(c+NUM_CHANS*(0)))] = triang_data[4+8*(out_b+NUM_BASELINES*(c))];
        reg_tile_data[2+4*(in_b+in_nbaseline*(c+NUM_CHANS*(1)))] = triang_data[5+8*(out_b+NUM_BASELINES*(c))];
        reg_tile_data[3+4*(in_b+in_nbaseline*(c+NUM_CHANS*(0)))] = triang_data[6+8*(out_b+NUM_BASELINES*(c))];
        reg_tile_data[3+4*(in_b+in_nbaseline*(c+NUM_CHANS*(1)))] = triang_data[7+8*(out_b+NUM_BASELINES*(c))];
      }  

    /*for (int i=0; i<10; ++i) {
    cout << triang_data[i] << " " << reg_tile_data[i] <<endl;
    cout << triang_data[32000+i] << " " << reg_tile_data[32000+i] <<endl;
    }*/

    // Write the REG_TILE data to file
    out_file.open(argv[3], ios::out|ios::binary|ios::app);
    if ( !out_file.is_open() ) {
      cerr << "Failed opening file " << argv[3] << endl;
      exit(1);
    }
    out_file.write((const char*)reg_tile_data, REG_TILE_FRAME_SIZE_BYTES);
    out_file.close();
  }
  return 0;
}



