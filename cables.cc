/*
    Copyright (C) 2016  Hugh Garsden

    This file is part of DuCT.

    DuCT is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    DuCT is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with DuCT.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <fstream>
#include <sstream>
#include <cstdlib>
#include <iostream>
#include <set>
#include "cables.h"



float Cable::extract_cable_len(string s) {
  int i=0, num=0;
  string num_str; 
  while ( isalpha(s[i]) ) ++i; ++i;		// Assuming EL_ at the start
  while ( i < s.size() ) { num_str += s[i]; ++i; }
  return atof(num_str.c_str()); 
}

int Cable::decodePolChar(int pol_char) {
    int temp;
    temp = toupper(pol_char);
    if ( temp == 'X' ) return 0;
    if ( temp == 'Y' ) return 1;
    return 99;
}


void Cables::print() {
  for (int i=0; i<list.size(); ++i) 
    cout << i << " " << list[i].stand_num() << " " << list[i].pol_index() << " " << list[i].cable_length() << " " << list[i].flag() << endl;
}



/******************************
 read the mapping between antennas and correlator inputs.
*******************************/
Cables::Cables(const char* filename) {
  ifstream file(filename);
  string line;
  vector <string> words;
  set<int> have_pol_X, have_pol_Y;


  if ( file.is_open() ) {     
    while( getline(file, line) ) 
      if ( line.size() > 0 && line[0] != '#' ) {
	words = split(line); 
        if ( words.size() != 5 ) {
          cerr <<  "Cables file invalid at line " << line << endl;
          exit(1);
        }

        Cable cbl(atoi(words[1].c_str()),cbl.extract_cable_len(words[3]),cbl.decodePolChar(words[2][0]),atoi(words[4].c_str()));

   	if ( cbl.stand_num() < 0 || NUM_STANDS <= cbl.stand_num() ) {
          cerr <<  "Invalid stand number " << cbl.stand_num() << " in instr_config file\n";
          exit(1);
        }	
	if ( words[2][0] == 'X' ) have_pol_X.insert(cbl.stand_num());
	else if ( words[2][0] == 'Y' ) have_pol_Y.insert(cbl.stand_num());
	else  { 
          cerr <<  "Invalid polarization specification: " << words[2][0] << ", in instr_config file\n";
          exit(1);
        }
	if ( !(cbl.flag() == 0 || cbl.flag() == 1 || cbl.flag() == 2) ) {
          cerr <<  "Invalid flag specification: " << cbl.flag() << ", in instr_config file\n";
          exit(1);
        }

	list.push_back(cbl);
        if ( atoi(words[0].c_str()) != list.size()-1 ) {
	  cerr <<  "Instr config input numbers are not in order at line " << list.size() <<  endl;  // Otherwise have to store them. 
          exit(1);
        }

      }
  } else { 
    cerr <<  "Failed to open instr_config file " << filename << endl;
    exit(1);
  }

  if ( list.size() != NUM_STANDS*2 ) {
    cerr <<  "Expecting " << NUM_STANDS*2 << " cable definitions in instr_config file\n";
    exit(1);
  }

  // check
  for (int i=0; i<NUM_STANDS; ++i) {

    if ( have_pol_X.count(i) == 0 ) {
      cerr <<  "Missing polarization X for stand" << i << " in instr_config file\n";
      exit(1);
    }
    if ( have_pol_Y.count(i) == 0 ) {
      cerr <<  "Missing polarization Y for stand" << i << " in instr_config file\n";
      exit(1);
    }
  }

  // insert fast lookup info
  for (int i=0; i<list.size(); ++i) {
    fast_length_table[list[i].stand_num()*2+list[i].pol_index()] = list[i].cable_length();
    fast_flag_table[list[i].stand_num()*2+list[i].pol_index()] = list[i].flag();
  }
}



/*
int main() {
  Cables list("../instr_config.tpl");
 
  list.print();
  cout << list.find_cable_length(168, 0) << " " << list.find_cable_length(248, 1) << endl;
  return 0;
}

*/
